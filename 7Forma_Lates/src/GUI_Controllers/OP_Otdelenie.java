//edit in bitbucket
package GUI_Controllers;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.Logger;

import ConnectManager.LoggerAllAplication;
import ConnectManager.SQLiteJDBC;
import application.Props;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.converter.DateStringConverter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class OP_Otdelenie implements Initializable{
	private static Logger log = LoggerAllAplication.getlogger();
	Connection conn = SQLiteJDBC.Get_Conn();
	private Stage thisdialogstage;
	private ObservableList<Integer> dataidotdelenia = FXCollections.observableArrayList();
	
	@FXML private DatePicker Data_IN;
	@FXML private DatePicker Data_OUT;
	@FXML private Button OK;
	@FXML private ComboBox<String> Otdelenie;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Data_OUT.setValue(LocalDate.now());
		Data_IN.setValue(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 1));

		Data_IN.getEditor().setTextFormatter(getTextFormatter());
		Data_OUT.getEditor().setTextFormatter(getTextFormatter());
		
		Otdelenie.setItems(FXCollections.observableArrayList(Get_List()));
		Otdelenie.getSelectionModel().select(0);
	}
	
	private TextFormatter<Date> getTextFormatter() {
		TextFormatter<Date> textFormatter = new TextFormatter<>(
				new DateStringConverter(new SimpleDateFormat("dd.MM.yyyy")), null, change -> {
					if (change.isAdded()) {
						if (change.getControlNewText().length() > 10) {
							return null;
						} else {
							if (!change.getText().matches("[0-9]") && (change.getControlNewText().length() != 10)) {
								return null;
							}
						}
						int caretPosition = change.getCaretPosition();
						if (caretPosition == 3 || caretPosition == 6) {
							change.setText("." + change.getText()/* + "." */);
							change.setCaretPosition(change.getControlNewText().length());
							change.setAnchor(change.getControlNewText().length());
						}
					}
					return change;
				});
		return textFormatter;
	}

	public Stage getdialogstage() {
		return thisdialogstage;
	}

	public void setDialogstage(Stage stage) {
		this.thisdialogstage = stage;		
	}
	@FXML public void KeyPress(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){
			getdialogstage().close();			
		}
	}

	@FXML
	public void on_OK(ActionEvent event) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String dateTime_IN = "'" + LocalDate.parse(Data_IN.getValue().toString(), formatter) + "'";
			String dateTime_OUT = "'" + LocalDate.parse(Data_OUT.getValue().toString(), formatter) + "'";
			String SQLQueary = "SELECT "
					+ "Otdelenie AS Otdelenie,ot.Koika AS Koika,IN_ALL,OUT_ALL,DIE,IN_Deti,OUT_Deti,OUT_DrSt,IN_Perevod,"
					+ "((SELECT Vsego FROM Registr WHERE id_Registr = ot.id_Registr)-"
					+ "(SELECT SUM(IN_ALL)-SUM(OUT_ALL)+SUM(IN_Perevod)-SUM(OUT_Perevod)-SUM(DIE) FROM Osnova WHERE id_OK=ot.id_OK AND "
					+ "Date BETWEEN " + dateTime_IN + " AND date('now')))*" + "(julianday(" + dateTime_OUT
					+ ")-julianday(" + dateTime_IN + ")+1)+IFNULL(Ostalnoe, 0)+"
					+ "CASE WHEN Koika LIKE '%пр.' THEN IFNULL(OUT_ALL, 0) ELSE 0 END AS KD,"
					+ "Chislo_Koek,Plan_KD " + "FROM  ("
					+ "SELECT okk.id_Koiki AS id_Koiki,id_Registr,Koika AS Koika,Name AS Otdelenie,id_OK AS id_OK,Chislo_Koek,Plan_KD "
					+ "FROM (SELECT * FROM Otdelenie WHERE "
					+ "id_Otdelenie="+dataidotdelenia.get(Otdelenie.getSelectionModel().getSelectedIndex())+") o INNER JOIN "
					+ "(SELECT id_Registr,k.id_Koiki AS id_Koiki,Name AS Koika,ok.id_OK AS id_OK,ok.id_Otdelenie AS  id_Otdelenie,"
					+ "Chislo_Koek,Plan_KD FROM Koiki k INNER JOIN (SELECT * FROM OK WHERE (Date_Open < " + dateTime_OUT + ") AND "
					+ "(Date_Close > " + dateTime_IN + ")) ok ON k.id_Koiki = ok.id_Koiki )  okk "
					+ "ON okk.id_Otdelenie = o.id_Otdelenie  ) ot LEFT JOIN (SELECT "
					+ "id_OK,SUM( IN_ALL) AS IN_ALL,SUM( OUT_ALL) AS OUT_ALL,SUM( DIE) AS DIE,SUM( IN_Deti) AS IN_Deti,"
					+ "SUM( OUT_Deti) AS OUT_Deti,SUM( OUT_DrSt) AS OUT_DrSt,SUM(Dney*Sostoit) AS Ostalnoe,SUM(IN_Perevod) AS IN_Perevod "
					+ "FROM (SELECT *,id_OK,julianday(" + dateTime_OUT + ")-julianday(Date)+1 AS Dney,"
					+ "IN_ALL-OUT_ALL+IN_Perevod-OUT_Perevod-DIE AS Sostoit "
					+ "FROM  (SELECT * FROM Osnova WHERE Date BETWEEN  " + dateTime_IN + " AND  " + dateTime_OUT + ")) "
					+ "GROUP BY id_OK) o ON ot.id_OK = o.id_OK GROUP BY Otdelenie,Koika";
			log.debug("OP_Otdelenie.on_OK\n"+SQLQueary);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("Data_IN", dateTime_IN.toString());
			parameters.put("Data_OUT", dateTime_OUT.toString());
			parameters.put("Path", Props.SOURCE_DIR_OTCHET + "logo.png");
			parameters.put("Otdelenie", Otdelenie.getSelectionModel().getSelectedItem());
			// JasperReport jasperReport =
			// JasperCompileManager.compileReport(Props.SOURCE_DIR_OTCHET+"Invoice.jrxml");
//			JasperDesign jasperDesign = JRXmlLoader.load(Props.SOURCE_DIR_OTCHET + "Invoice.jrxml");
			JasperDesign jasperDesign = JRXmlLoader.load(Props.SOURCE_DIR_OTCHET + "OP_Otdelenie.jrxml");
			JRDesignQuery newQuery = new JRDesignQuery();
			newQuery.setText(SQLQueary);
			jasperDesign.setQuery(newQuery);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);
			JasperViewer visor = new JasperViewer(jasperPrint, false);
			visor.setVisible(true);

		} catch (JRException e) {
			JOptionPane.showMessageDialog(null, "bla\n" + e.getMessage());
			log.info(e);
		}
	}
	
	//@SuppressWarnings("unused")
	private void printReport(String resourcefile){
		  try {
		    JasperReport jr;
		    InputStream in=getClass().getResourceAsStream(resourcefile + ".ser");
		    if (in == null) {
		      JasperDesign jd=JRXmlLoader.load(getClass().getResourceAsStream(resourcefile + ".jrxml"));
		      jr=JasperCompileManager.compileReport(jd);
		    }
		 else {
		      ObjectInputStream oin=new ObjectInputStream(in);
		      jr=(JasperReport)oin.readObject();
		      oin.close();
		    }
		    Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("Data_IN", Data_IN.getValue().toString());
			parameters.put("Data_OUT", Data_OUT.getValue().toString());	

		  }
		 catch (  Exception e) {
			 JOptionPane.showMessageDialog(null,e.getMessage());
			 log.info(e);
		  }
		}

	private ObservableList<String> Get_List() {
		try {
			dataidotdelenia.clear();
			String Queary = "SELECT Name,id_Otdelenie FROM Otdelenie GROUP BY Name";
			ResultSet rs = conn.createStatement().executeQuery(Queary);
			ObservableList<String> row = FXCollections.observableArrayList();
			while (rs.next()) {
				row.add(rs.getString(1));
				dataidotdelenia.add(Integer.valueOf(rs.getInt(2)));
			}
			return row;

		} catch (SQLException e) {
			log.error(e);
			return null;
		}
	}

	@FXML public void onDown(ActionEvent event) {
		Data_IN.setValue(Data_IN.getValue().minusMonths(1));
		Data_OUT.setValue(Data_OUT.getValue().minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()));
	}

	@FXML public void onUP(ActionEvent event) {
		Data_IN.setValue(Data_IN.getValue().plusMonths(1));
		Data_OUT.setValue(Data_OUT.getValue().plusMonths(1).with(TemporalAdjusters.lastDayOfMonth()));
	}
}
