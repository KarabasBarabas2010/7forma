//edit in bitbucket
package GUI_Controllers;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.Logger;

import ConnectManager.LoggerAllAplication;
import ConnectManager.SQLiteJDBC;
import application.Props;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.converter.DateStringConverter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class OP implements Initializable{
	private static Logger log = LoggerAllAplication.getlogger();
	Connection conn = SQLiteJDBC.Get_Conn();
	private Stage thisdialogstage;
	
	@FXML private DatePicker Data_IN;
	@FXML private DatePicker Data_OUT;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Data_OUT.setValue(LocalDate.now());
		Data_IN.setValue(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 1));

		Data_IN.getEditor().setTextFormatter(getTextFormatter());
		Data_OUT.getEditor().setTextFormatter(getTextFormatter());
		
	}
	
	private TextFormatter<Date> getTextFormatter() {
		TextFormatter<Date> textFormatter = new TextFormatter<>(
				new DateStringConverter(new SimpleDateFormat("dd.MM.yyyy")), null, change -> {
					if (change.isAdded()) {
						if (change.getControlNewText().length() > 10) {
							return null;
						} else {
							if (!change.getText().matches("[0-9]") && (change.getControlNewText().length() != 10)) {
								return null;
							}
						}
						int caretPosition = change.getCaretPosition();
						if (caretPosition == 3 || caretPosition == 6) {
							change.setText("." + change.getText()/* + "." */);
							change.setCaretPosition(change.getControlNewText().length());
							change.setAnchor(change.getControlNewText().length());
						}
					}
					return change;
				});
		return textFormatter;
	}

	public Stage getdialogstage() {
		return thisdialogstage;
	}

	public void setDialogstage(Stage stage) {
		this.thisdialogstage = stage;		
	}
	@FXML public void KeyPress(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){
			getdialogstage().close();			
		}
	}

	@FXML
	public void on_OK(ActionEvent event) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String dateTime_IN = "'" + LocalDate.parse(Data_IN.getValue().toString(), formatter) + "'";
			String dateTime_OUT = "'" + LocalDate.parse(Data_OUT.getValue().toString(), formatter) + "'";
			String SQLQueary = "SELECT "
					+ "Otdelenie AS Otdelenie,ot.Koika AS Koika,IN_ALL,OUT_ALL,DIE,IN_Deti,OUT_Deti,OUT_DrSt,IN_Perevod,IN_Perevod_Deti,"
					+ "((SELECT Vsego FROM Registr WHERE id_Registr = ot.id_Registr)-"
					+ "(SELECT SUM(IN_ALL)-SUM(OUT_ALL)+SUM(IN_Perevod)-SUM(OUT_Perevod)-SUM(DIE) FROM Osnova WHERE id_OK=ot.id_OK AND "
					+ "Date BETWEEN " + dateTime_IN + " AND date('now')))*" + "(julianday(" + dateTime_OUT
					+ ")-julianday(" + dateTime_IN + ")+1)+IFNULL(Ostalnoe, 0)+"
					+ "CASE WHEN Koika LIKE '%пр.' THEN IFNULL(OUT_ALL, 0) ELSE 0 END AS KD," 
					+ "Chislo_Koek,Plan_KD " + "FROM  ("
					+ "SELECT okk.id_Koiki AS id_Koiki,id_Registr,Koika AS Koika,Name AS Otdelenie,id_OK AS id_OK,Chislo_Koek,Plan_KD "
					+ "FROM Otdelenie o INNER JOIN "
					+ "(SELECT id_Registr,k.id_Koiki AS id_Koiki,Name AS Koika,ok.id_OK AS id_OK,ok.id_Otdelenie AS  id_Otdelenie,"
					+ "Chislo_Koek,IFNULL(Plan_KD,0) AS Plan_KD FROM Koiki k INNER JOIN "
					+ "(SELECT id_Registr,Chislo_Koek,id_Otdelenie,id_Koiki,OK.id_OK AS id_OK,p.Plan_KD AS Plan_KD "
					+ "FROM OK INNER JOIN ( "
					+ CreatePlan_KD(LocalDate.parse(Data_IN.getValue().toString(), formatter).getMonthValue(),
							LocalDate.parse(Data_OUT.getValue().toString(), formatter).getMonthValue(),
							LocalDate.parse(Data_IN.getValue().toString(), formatter).getYear(),
							LocalDate.parse(Data_OUT.getValue().toString(), formatter).getYear(),
							LocalDate.parse(Data_OUT.getValue().toString(), formatter).lengthOfMonth(),
							LocalDate.parse(Data_OUT.getValue().toString(), formatter).getDayOfMonth(),
							LocalDate.parse(Data_IN.getValue().toString(), formatter).lengthOfMonth(),
							LocalDate.parse(Data_IN.getValue().toString(), formatter).getDayOfMonth())
//					+ "Month="+LocalDate.parse(Data_IN.getValue().toString(), formatter).getMonthValue()
//					+ " AND Year="+LocalDate.parse(Data_IN.getValue().toString(), formatter).getYear()+") "
					+ ") p ON OK.id_OK = p.id_OK WHERE (Date_Open < " + dateTime_OUT + ") AND (Date_Close  > " + dateTime_IN + ")) ok "
					+ "ON k.id_Koiki = ok.id_Koiki)  okk ON okk.id_Otdelenie = o.id_Otdelenie  ) ot LEFT JOIN (SELECT "
					+ "id_OK,SUM( IN_ALL) AS IN_ALL,SUM( OUT_ALL) AS OUT_ALL,SUM( DIE) AS DIE,SUM( IN_Deti) AS IN_Deti,"
					+ "SUM( OUT_Deti) AS OUT_Deti,SUM( OUT_DrSt) AS OUT_DrSt,SUM(Dney*Sostoit) AS Ostalnoe, SUM(IN_Perevod) AS IN_Perevod, SUM(IN_Perevod_Deti) AS IN_Perevod_Deti "
					+ "FROM (SELECT *,id_OK,julianday(" + dateTime_OUT + ")-julianday(Date)+1 AS Dney,"
					+ "IN_ALL-OUT_ALL+IN_Perevod-OUT_Perevod-DIE AS Sostoit "
					+ "FROM  (SELECT * FROM Osnova WHERE Date BETWEEN  " + dateTime_IN + " AND  " + dateTime_OUT + ")) "
					+ "GROUP BY id_OK) o ON ot.id_OK = o.id_OK GROUP BY Otdelenie,Koika";
			log.debug("OP.on_OK\n"+SQLQueary);

			Map<String, Object> parameters = new HashMap<String, Object>();
			log.debug(Data_IN.getValue().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
			parameters.put("Data_IN",Data_IN.getValue().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
			parameters.put("Data_OUT",Data_OUT.getValue().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
			parameters.put("Path", Props.SOURCE_DIR_OTCHET + "logo.png");
			JasperDesign jasperDesign = JRXmlLoader.load(Props.SOURCE_DIR_OTCHET + "OP.jrxml");
			JRDesignQuery newQuery = new JRDesignQuery();
			newQuery.setText(SQLQueary);
			jasperDesign.setQuery(newQuery);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);
			JasperViewer visor = new JasperViewer(jasperPrint, false);
			visor.setVisible(true);

		} catch (JRException e) {
			JOptionPane.showMessageDialog(null, "Err\n" + e.getMessage());
			log.error(e);
		}
	}
	private String CreatePlan_KD(int Month_IN,int Month_OUT,int Year_IN,int Year_OUT,int length,int day,int length_IN,int day_IN){
		log.debug("Month_IN="+Month_IN+"\nMonth_OUT="+Month_OUT+"\nYear_IN="+Year_IN+"\nYear_OUT="+Year_OUT+"\nLenghtOfMonth="+length+"\nDayOfMonth="+day+"\nlength_IN="+length_IN+"\nday_IN="+day_IN);
		String s="";
		s="SELECT  id_OK,SUM(CASE WHEN ((Year="+Year_IN+") AND (Month="+Month_IN+")) THEN Plan_KD*"+(Year_IN==Year_OUT&&Month_IN==Month_OUT ? day-day_IN+1 : length_IN-day_IN+1)+"/"+length_IN+" ELSE "
				+ "CASE WHEN ((Year="+Year_OUT+") AND (Month="+Month_OUT+")) THEN Plan_KD*"+day+"/"+length+" ELSE Plan_KD END END) AS Plan_KD FROM Plan_KD "
						+ "WHERE Year="+Year_IN+" AND Month IN ("+Month_IN;
		int k=12;
		if(Year_IN==Year_OUT) {
			k=Month_OUT;
		}
		for(int i=Month_IN+1;i<=k;i++) {
			s=s+","+i;
		}
		s=s+")";
		for(int i=1;i<=Year_OUT-Year_IN;i++) {
			if(Month_OUT!=1) {
				s=s+" OR Year="+(Year_IN+i)+" AND Month IN (1";
			}
			for(int j=2;j<=Month_OUT;j++) {
				s=s+","+j;					
			}
			s=s+")";
		}
		s=s+" GROUP BY id_OK ";
		
		//Предыдущий вариант
		/*log.debug("Month_IN="+Month_IN+"\nMonth_OUT="+Month_OUT+"\nYear_IN="+Year_IN+"\nYear_OUT="+Year_OUT+"\nLenghtOfMonth="+length+"\nDayOfMonth="+day);
		String s="";
		if((Month_IN==Month_OUT)&&(Year_IN==Year_OUT)) {
			s="SELECT  id_OK, Plan_KD*"+(day-day_IN+1)+"/"+(length)+" AS Plan_KD FROM Plan_KD WHERE Month="+Month_IN+" AND Year="+Year_IN+" GROUP BY id_OK";
		} else {
			if(Year_IN==Year_OUT) {
				if(Month_OUT-Month_IN==1) {
					s="SELECT id_OK,(SELECT Plan_KD FROM Plan_KD WHERE Year="+Year_IN+" AND Month="+Month_IN+")*"+(length_IN-day_IN+1)+"/"+length_IN
							+"+Plan_KD*"+day+"/"+length+" AS Plan_KD FROM Plan_KD WHERE Year="+Year_OUT+" AND Month="+Month_OUT+" GROUP BY id_OK";
				} else {
					s="SELECT  id_OK, SUM(Plan_KD)+(SELECT Plan_KD FROM Plan_KD WHERE Month="+Month_OUT
						+" AND Year="+Year_OUT+")*"+day+"/"+length+"+ (SELECT Plan_KD FROM Plan_KD WHERE Month="+Month_IN
						+" AND Year="+Year_IN+")*"+(length_IN-day_IN+1)+"/"+length_IN+" AS Plan_KD FROM Plan_KD WHERE Month IN("+(Month_IN+1);
					for(int i=Month_IN+2;i<Month_OUT;i++) {
						s=s+","+i;
					}
					s=s+") AND Year="+Year_IN+" GROUP BY id_OK";
				}
			} else {	
				if((Month_IN==12)&&(Month_OUT==1)&&(Year_OUT-Year_IN==1)) {
					s="SELECT id_OK,(SELECT Plan_KD FROM Plan_KD WHERE Year="+Year_IN+" AND Month="+Month_IN+")*"+(length_IN-day_IN+1)+"/"+length_IN
							+"+Plan_KD*"+day+"/"+length+" AS Plan_KD FROM Plan_KD WHERE Year="+Year_OUT+" AND Month="+Month_OUT+" GROUP BY id_OK";					
				} else {
					s="SELECT  id_OK, SUM(Plan_KD)+(SELECT Plan_KD FROM Plan_KD WHERE Month="+Month_OUT
						+" AND Year="+Year_OUT+")*"+day+"/"+length+" AS Plan_KD FROM Plan_KD WHERE (Year="+Year_IN+" AND Month IN ("+Month_IN;
					for(int i=Month_IN+1;i<=12;i++) {
						s=s+","+i;
					}
					s=s+")";
					for(int i=1;i<=Year_OUT-Year_IN;i++) {
						if(Month_OUT!=1) {
							s=s+" OR (Year="+(Year_IN+i)+" AND Month IN (1";
						}
						int k=12;
						if(i==Year_OUT-Year_IN) {
							k=Month_OUT-1;
						}
						for(int j=2;j<=k;j++) {
							s=s+","+j;					
						}
						s=s+")";
					}
					s=s+" GROUP BY id_OK ";
				}
			}
		}*/
		
		//Старый вариант
		/*String s="Month IN ("+Month_IN;
		for(int i=1;i<=(Month_OUT-Month_IN);i++){
			s=s+","+(Month_IN+i);
		}
		s=s+") AND Year IN ("+Year_IN;
		for(int i=0;i<(Year_OUT-Year_IN);i++){
			s=s+","+(Year_IN+i+1);
		}
		s=s+") ";*/
		log.debug("OP.CreatePlan_KD\n"+s);
		return s;
		
	}
	
	
	//@SuppressWarnings("unused")
	private void printReport(String resourcefile){
		  try {
		    JasperReport jr;
		    InputStream in=getClass().getResourceAsStream(resourcefile + ".ser");
		    if (in == null) {
		      JasperDesign jd=JRXmlLoader.load(getClass().getResourceAsStream(resourcefile + ".jrxml"));
		      jr=JasperCompileManager.compileReport(jd);
		    }
		 else {
		      ObjectInputStream oin=new ObjectInputStream(in);
		      jr=(JasperReport)oin.readObject();
		      oin.close();
		    }
		    Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("Data_IN", Data_IN.getValue().toString());
			parameters.put("Data_OUT", Data_OUT.getValue().toString());	

		  }
		 catch (  Exception e) {
			 JOptionPane.showMessageDialog(null,e.getMessage());
			 log.info(e);
		  }
		}

	@FXML public void onDown(ActionEvent event) {
		Data_IN.setValue(Data_IN.getValue().minusMonths(1));
		Data_OUT.setValue(Data_OUT.getValue().minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()));
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//		LocalDate.parse(Data_IN.getValue().toString(), formatter) 
//		calendar.setValue(calendar.getValue().plusDays(1));
	}

	@FXML public void onUP(ActionEvent event) {
		Data_IN.setValue(Data_IN.getValue().plusMonths(1));
		Data_OUT.setValue(Data_OUT.getValue().plusMonths(1).with(TemporalAdjusters.lastDayOfMonth()));
	}
}
