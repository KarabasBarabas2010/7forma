package application;
	
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

//import org.apache.log4j.BasicConfigurator;
//import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.Logger;
import ConnectManager.LoggerAllAplication;
import GUI_Controllers.Osnova;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;


public class Main extends Application {
	
	//private static Logger log = Logger.getLogger(Main.class.getName());
	private static Logger log = LoggerAllAplication.getlogger();
	
	
	@Override
	public void start(Stage primaryStage) {
		try {
			
			//BasicConfigurator.configure();		
			//String log4jConfPath = "log4j.properties";
			//PropertyConfigurator.configure(log4jConfPath);
			log.info("Запуск");
			
			//log4j.rootLogger=INFO, file, stdout
//					log4j.appender.stdout=org.apache.log4j.ConsoleAppender
//					log4j.appender.stdout.Target=System.out
//					log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
//					log4j.appender.stdout.layout.ConversionPattern=%d{yy/MM/dd HH:mm:ss} %p %c{2}: %m%n
			
			
			URL resourcePath = getClass().getClassLoader().getResource("");
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/Osnova.fxml"));
			
			Parent root = (Parent)loader.load();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(resourcePath+"application.css");			
			primaryStage.setScene(scene);
			Osnova controller = (Osnova)loader.getController();
			controller.setThisdialogstage(primaryStage);
			primaryStage.getIcons().add(new Image(resourcePath+"images/medical_icon.png"));
			primaryStage.setTitle("7 Форма");
			primaryStage.show();
			log.info("Выключение");
		} catch(Exception e) {
			log.error(e);
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			new Props();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		launch(args);
	}
}
