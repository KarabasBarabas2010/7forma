package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

import org.eclipse.jdt.internal.compiler.batch.FileSystem;

public class Props {
	public static String PATH_TO_BD;
	public static String JDBC_BD;
	public static String CONNECT_BD;
	public static String SOURCE_DIR_OTCHET;
	
	public Props() throws FileNotFoundException, IOException, URISyntaxException {
		String path = getClass().getClassLoader().getResource("").toURI().getPath();
		path=path+File.separator;
		Properties props = new Properties();
		props.load(new FileInputStream(new File(path+"Property.ini")));
		PATH_TO_BD=props.getProperty("PATH_TO_BD");
		JDBC_BD=props.getProperty("JDBC_BD");
		CONNECT_BD=props.getProperty("CONNECT_BD");
		SOURCE_DIR_OTCHET=props.getProperty("SOURCE_DIR_OTCHET");
	}

}
