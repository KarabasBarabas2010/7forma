//edit in bitbucket
package GUI_Controllers;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.Logger;

import ConnectManager.LoggerAllAplication;
import ConnectManager.SQLiteJDBC;
import application.Props;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.converter.DateStringConverter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class SV implements Initializable{
	private static Logger log = LoggerAllAplication.getlogger();
	Connection conn = SQLiteJDBC.Get_Conn();
	private Stage thisdialogstage;
	
	@FXML private DatePicker Data_IN;
	@FXML private DatePicker Data_OUT;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Data_OUT.setValue(LocalDate.now());
		Data_IN.setValue(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 1));

		Data_IN.getEditor().setTextFormatter(getTextFormatter());
		Data_OUT.getEditor().setTextFormatter(getTextFormatter());
		
	}
	
	private TextFormatter<Date> getTextFormatter() {
		TextFormatter<Date> textFormatter = new TextFormatter<>(
				new DateStringConverter(new SimpleDateFormat("dd.MM.yyyy")), null, change -> {
					if (change.isAdded()) {
						if (change.getControlNewText().length() > 10) {
							return null;
						} else {
							if (!change.getText().matches("[0-9]") && (change.getControlNewText().length() != 10)) {
								return null;
							}
						}
						int caretPosition = change.getCaretPosition();
						if (caretPosition == 3 || caretPosition == 6) {
							change.setText("." + change.getText()/* + "." */);
							change.setCaretPosition(change.getControlNewText().length());
							change.setAnchor(change.getControlNewText().length());
						}
					}
					return change;
				});
		return textFormatter;
	}

	public Stage getdialogstage() {
		return thisdialogstage;
	}

	public void setDialogstage(Stage stage) {
		this.thisdialogstage = stage;		
	}
	@FXML public void KeyPress(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){
			getdialogstage().close();			
		}
	}

	@FXML
	public void on_OK(ActionEvent event) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String dateTime_IN = "'" + LocalDate.parse(Data_IN.getValue().toString(), formatter) + "'";
			String dateTime_OUT = "'" + LocalDate.parse(Data_OUT.getValue().toString(), formatter) + "'";
			String SQLQueary="SELECT Otdelenie,Koika,IFNULL(IN_ALL, 0) AS IN_ALL,IFNULL(IN_DS, 0) AS IN_DS,IFNULL(IN_Selo, 0) AS IN_Selo,IFNULL(IN_Deti, 0) AS IN_Deti,IFNULL(IN_Mother, 0) AS IN_Mother,IFNULL(OUT_ALL, 0) AS OUT_ALL,IFNULL(OUT_DS, 0) AS OUT_DS,IFNULL(OUT_Selo, 0) AS OUT_Selo,IFNULL(OUT_Deti, 0) AS OUT_Deti,IFNULL(OUT_Mother, 0) AS OUT_Mother"
					+ ",IFNULL(OUT_DrSt, 0) AS OUT_DrSt,IFNULL(IN_Perevod, 0) AS IN_Perevod,IFNULL(IN_Perevod_Deti, 0) AS IN_Perevod_Deti,IFNULL(IN_Perevod_Mother, 0) AS IN_Perevod_Mother,IFNULL(OUT_Perevod, 0) AS OUT_Perevod,IFNULL(OUT_Perevod_Deti, 0) AS OUT_Perevod_Deti,IFNULL(OUT_Perevod_Mother, 0) AS OUT_Perevod_Mother"
					+ ",IFNULL(DIE, 0) AS DIE,IFNULL(DIE_Deti, 0) AS DIE_Deti,Vsego,Vsego_Mother,Vsego_Deti,Vsego_Selo,CASE WHEN Koika   LIKE    '%пр.' THEN (("
					+ "SELECT Vsego FROM Registr WHERE id_Registr = ot.id_Registr)-IFNULL(("
					+ "SELECT SUM(IN_ALL)-SUM(OUT_ALL)+SUM(IN_Perevod)-SUM(OUT_Perevod)-SUM(DIE) FROM Osnova WHERE id_OK = ot.id_OK "
					+ "AND Date    BETWEEN "+dateTime_IN+" AND date('now')),0))*(julianday("+dateTime_OUT+ ")-julianday("+dateTime_IN+")+1)"
					+ "+IFNULL(Ostalnoe, 0)+IFNULL(OUT_ALL, 0) ELSE ((SELECT Vsego FROM Registr WHERE "
					+ "id_Registr  =   ot.id_Registr)-IFNULL((SELECT SUM(IN_ALL)-SUM(OUT_ALL)+SUM(IN_Perevod)-SUM(OUT_Perevod)-SUM(DIE) "
					+ "FROM Osnova WHERE id_OK = ot.id_OK AND Date    BETWEEN "+dateTime_IN+ " AND date('now')),0))*"
					+ "(julianday("+dateTime_OUT+")-julianday("+dateTime_IN+")+1)+IFNULL(Ostalnoe, 0) END AS  KD "
					+ ",((SELECT Vsego_Mother FROM Registr WHERE id_Registr = ot.id_Registr)-IFNULL((SELECT "
					+ "SUM(IN_Mother)-SUM(OUT_Mother)+SUM(IN_Perevod_Mother)-SUM(OUT_Perevod_Mother) FROM Osnova WHERE "
					+ "id_OK = ot.id_OK AND Date    BETWEEN "+dateTime_IN+" AND date('now')),0))* (julianday("+dateTime_OUT
					+ ")-julianday("+dateTime_IN+")+1)+IFNULL(Ostalnoe_Mother, 0) AS  KD_Mother,((SELECT Vsego_Deti FROM Registr WHERE "
					+ "id_Registr  =   ot.id_Registr)-IFNULL((SELECT SUM(IN_Deti)-SUM(OUT_Deti)+SUM(IN_Perevod_Deti)-SUM(OUT_Perevod_Deti) "
					+ "FROM Osnova WHERE id_OK = ot.id_OK AND Date BETWEEN "+dateTime_IN+" AND date('now')),0))*"
					+ "(julianday("+dateTime_OUT+")-julianday("+dateTime_IN+")+1)+IFNULL(Ostalnoe_Deti, 0) AS  KD_Deti FROM ((SELECT "
					+ "okk.id_Koiki AS id_Koiki,id_Registr,Koika AS Koika,Name AS Otdelenie,id_OK AS id_OK,Chislo_Koek,Plan_KD FROM "
					+ "Otdelenie o INNER JOIN (SELECT id_Registr,k.id_Koiki AS id_Koiki,Name AS Koika,ok.id_OK AS id_OK,"
					+ "ok.id_Otdelenie AS id_Otdelenie,Chislo_Koek,Plan_KD FROM Koiki k INNER JOIN (SELECT id_Registr,"
					+ "Chislo_Koek,id_Otdelenie,id_Koiki,OK.id_OK AS id_OK,p.Plan_KD AS Plan_KD FROM OK INNER JOIN (SELECT "
					+ "id_OK,SUM(Plan_KD)/COUNT(Plan_KD) AS Plan_KD FROM Plan_KD WHERE "
					+ CreateWhereForPlan_KD(LocalDate.parse(Data_IN.getValue().toString(), formatter).getMonthValue(),
							LocalDate.parse(Data_OUT.getValue().toString(), formatter).getMonthValue(),
							LocalDate.parse(Data_IN.getValue().toString(), formatter).getYear(),
							LocalDate.parse(Data_OUT.getValue().toString(), formatter).getYear())
					+ "GROUP BY id_OK ) p ON OK.id_OK = p.id_OK ) ok ON k.id_Koiki = ok.id_Koiki ) okk ON "
					+ "okk.id_Otdelenie = o.id_Otdelenie ) ot LEFT JOIN ( SELECT id_OK,SUM(IN_ALL) AS IN_ALL,"
					+ "SUM(IN_DS) AS IN_DS,SUM(IN_Selo) AS IN_Selo,SUM(IN_Deti) AS IN_Deti,SUM(IN_Mother) AS IN_Mother,"
					+ "SUM(OUT_ALL) AS OUT_ALL,SUM(OUT_DS) AS OUT_DS,SUM(OUT_Selo) AS  OUT_Selo,SUM(OUT_Deti) AS OUT_Deti,"
					+ "SUM(OUT_Mother) AS OUT_Mother,SUM(OUT_DrSt) AS OUT_DrSt,SUM(IN_Perevod) AS IN_Perevod,"
					+ "SUM(IN_Perevod_Deti) AS IN_Perevod_Deti,SUM(IN_Perevod_Mother) AS IN_Perevod_Mother,"
					+ "SUM(OUT_Perevod) AS OUT_Perevod,SUM(OUT_Perevod_Deti) AS OUT_Perevod_Deti,"
					+ "SUM(OUT_Perevod_Mother) AS OUT_Perevod_Mother,SUM(DIE) AS DIE,SUM(DIE_Deti) AS DIE_Deti,SUM(Dney*Izm) AS Ostalnoe,"
					+ "SUM(Dney*Izm_Mother) AS Ostalnoe_Mother,SUM(Dney*Izm_Deti) AS Ostalnoe_Deti FROM ( SELECT *,"
					+ "id_OK,julianday("+dateTime_OUT+")-julianday(Date)+1 AS Dney,IN_ALL-OUT_ALL+IN_Perevod-OUT_Perevod-DIE AS Izm,"
					+ "IN_Mother-OUT_Mother+IN_Perevod_Mother-OUT_Perevod_Mother AS Izm_Mother,"
					+ "IN_Deti-OUT_Deti+IN_Perevod_Deti-OUT_Perevod_Deti AS Izm_Deti FROM ( SELECT * FROM Osnova WHERE Date "
					+ "BETWEEN "+dateTime_IN+" AND "+dateTime_OUT+" )) GROUP BY id_OK ) o ON ot.id_OK=o.id_OK) oot LEFT JOIN ( SELECT "
					+ "Vsego-ifnull(IN_ALL, 0)+ifnull(OUT_ALL, 0)-ifnull(IN_Perevod, 0)+ifnull(OUT_Perevod, 0)+ifnull(DIE, 0) AS  Vsego,"
					+ "Vsego_Deti-ifnull(IN_Deti, 0)+ifnull(OUT_Deti, 0)-ifnull(IN_Perevod_Deti, 0)+ifnull(OUT_Perevod_Deti, 0)+"
					+ "ifnull(DIE_Deti, 0) AS Vsego_Deti,Vsego_Selo-ifnull(IN_Selo, 0)+ifnull(OUT_Selo, 0) AS Vsego_Selo,"
					+ "Vsego_Mother-ifnull(IN_Mother, 0)+ifnull(OUT_Mother, 0)-ifnull(IN_Perevod_Mother, 0)+"
					+ "ifnull(OUT_Perevod_Mother, 0) AS Vsego_Mother,reg.id_OK AS id_OK FROM ( SELECT Vsego,Vsego_Deti,Vsego_Selo,"
					+ "Vsego_Mother,id_OK FROM ( SELECT id_OK,id_Registr FROM OK) ok INNER JOIN ( SELECT Vsego,Vsego_Deti,Vsego_Selo,"
					+ "Vsego_Mother,id_Registr FROM Registr ) r ON r.id_Registr = ok.id_Registr ) reg LEFT JOIN ( SELECT id_OK,"
					+ "SUM(IN_ALL) AS IN_ALL,SUM(IN_Deti) AS IN_Deti,SUM(IN_Selo) AS IN_Selo,SUM(IN_Mother) AS IN_Mother,"
					+ "SUM(IN_Perevod) AS IN_Perevod,SUM(IN_Perevod_Deti) AS IN_Perevod_Deti,SUM(IN_Perevod_Mother) AS IN_Perevod_Mother,"
					+ "SUM(OUT_ALL) AS OUT_ALL,SUM(OUT_Deti) AS OUT_Deti,SUM(OUT_Selo) AS OUT_Selo,SUM(OUT_Mother) AS OUT_Mother,"
					+ "SUM(OUT_Perevod) AS OUT_Perevod,SUM(OUT_Perevod_Deti) AS OUT_Perevod_Deti,"
					+ "SUM(OUT_Perevod_Mother) AS OUT_Perevod_Mother,SUM(DIE) AS DIE,SUM(DIE_Deti) AS DIE_Deti FROM Osnova WHERE "
					+ "Date >= "+dateTime_IN+" GROUP BY id_OK ) osn ON reg.id_OK   =   osn.id_OK ) v ON oot.id_OK = v.id_OK GROUP BY Otdelenie,Koika";
			log.debug("SV.on_OK\n"+SQLQueary);

			Map<String, Object> parameters = new HashMap<String, Object>();
				log.debug("Map creat");
				log.debug("Data_IN = "+Data_IN.getValue().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
				log.debug("Data_OUT = "+Data_OUT.getValue().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
				log.debug("Path = "+Props.SOURCE_DIR_OTCHET + "logo.png");
			parameters.put("Data_IN",Data_IN.getValue().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
			parameters.put("Data_OUT",Data_OUT.getValue().format(DateTimeFormatter.ofPattern("dd MMMM yyyy")));
			parameters.put("Path", Props.SOURCE_DIR_OTCHET + "logo.png");
				log.debug("Parameters creat");
			JasperDesign jasperDesign = JRXmlLoader.load(Props.SOURCE_DIR_OTCHET + "SV.jrxml");
				log.debug("JasperDesign load");
			JRDesignQuery newQuery = new JRDesignQuery();
				log.debug("JRDesignQuery newQuery creat");
			newQuery.setText(SQLQueary);
				log.debug("newQuery set text");
			jasperDesign.setQuery(newQuery);
				log.debug("jasperDesign setQuery");
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
				log.debug("JasperReport compileReport");
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);
				log.debug("JasperPrint fillReport");
			JasperViewer visor = new JasperViewer(jasperPrint, false);
				log.debug("JasperViewer new creat");
			visor.setVisible(true);

		} catch (JRException e) {
			JOptionPane.showMessageDialog(null, "Error in JRException ...\n" + e.getMessage());
		}
	}
	private String CreateWhereForPlan_KD(int Month_IN,int Month_OUT,int Year_IN,int Year_OUT){
		log.debug("Month_IN="+Month_IN+"\nMonth_OUT="+Month_OUT+"\nYear_IN="+Year_IN+"\nYear_OUT="+Year_OUT);
		String s="Month IN ("+Month_IN;
		for(int i=1;i<=(Month_OUT-Month_IN);i++){
			s=s+","+(Month_IN+i);
		}
		s=s+") AND Year IN ("+Year_IN;
		for(int i=0;i<(Year_OUT-Year_IN);i++){
			s=s+","+(Year_IN+i+1);
		}
		s=s+") ";
		return s;
		
	}
	
	
	//@SuppressWarnings("unused")
	private void printReport(String resourcefile){
		  try {
		    JasperReport jr;
		    InputStream in=getClass().getResourceAsStream(resourcefile + ".ser");
		    if (in == null) {
		      JasperDesign jd=JRXmlLoader.load(getClass().getResourceAsStream(resourcefile + ".jrxml"));
		      jr=JasperCompileManager.compileReport(jd);
		    }
		 else {
		      ObjectInputStream oin=new ObjectInputStream(in);
		      jr=(JasperReport)oin.readObject();
		      oin.close();
		    }
		    Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("Data_IN", Data_IN.getValue().toString());
			parameters.put("Data_OUT", Data_OUT.getValue().toString());	

		  }
		 catch (  Exception e) {
			 JOptionPane.showMessageDialog(null,e.getMessage());

		  }
		}

	@FXML public void onDown(ActionEvent event) {
		Data_IN.setValue(Data_IN.getValue().minusMonths(1));
		Data_OUT.setValue(Data_OUT.getValue().minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()));
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//		LocalDate.parse(Data_IN.getValue().toString(), formatter) 
//		calendar.setValue(calendar.getValue().plusDays(1));
	}

	@FXML public void onUP(ActionEvent event) {
		Data_IN.setValue(Data_IN.getValue().plusMonths(1));
		Data_OUT.setValue(Data_OUT.getValue().plusMonths(1).with(TemporalAdjusters.lastDayOfMonth()));
	}
}
