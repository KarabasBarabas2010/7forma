package Components;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class tnu_ochen_nujna9 {
	
	private SimpleStringProperty Name = new SimpleStringProperty();
	public String getName(){return Name.get();}
	public void setName(String value){this.Name.set(value);}
	
	private SimpleIntegerProperty IN_All = new SimpleIntegerProperty();
	public int getIN_All(){return IN_All.get();}
	public void setIN_All(int value){this.IN_All.set(value);}
	
	private SimpleIntegerProperty IN_DS = new SimpleIntegerProperty();
	public int getIN_DS(){return IN_DS.get();}
	public void setIN_DS(int value){this.IN_DS.set(value);}
	
	private SimpleIntegerProperty IN_Selo = new SimpleIntegerProperty();
	public int getIN_Selo(){return IN_Selo.get();}
	public void setIN_Selo(int value){this.IN_Selo.set(value);}
	
	private SimpleIntegerProperty OUT_All = new SimpleIntegerProperty();
	public int getOUT_All(){return OUT_All.get();}
	public void setOUT_All(int value){this.OUT_All.set(value);}
	
	private SimpleIntegerProperty OUT_DS = new SimpleIntegerProperty();
	public int getOUT_DS(){return OUT_DS.get();}
	public void setOUT_DS(int value){this.OUT_DS.set(value);}
	
	private SimpleIntegerProperty OUT_DrSt = new SimpleIntegerProperty();
	public int getOUT_DrSt(){return OUT_DrSt.get();}
	public void setOUT_DrSt(int value){this.OUT_DrSt.set(value);}
	
	private SimpleIntegerProperty OUT_Selo = new SimpleIntegerProperty();
	public int getOUT_Selo(){return OUT_Selo.get();}
	public void setOUT_Selo(int value){this.OUT_Selo.set(value);}
	
	private SimpleIntegerProperty IN_Perevod = new SimpleIntegerProperty();
	public int getIN_Perevod(){return IN_Perevod.get();}
	public void setIN_Perevod(int value){this.IN_Perevod.set(value);}
	
	private SimpleIntegerProperty IN_Perevod_Deti = new SimpleIntegerProperty();
	public int getIN_Perevod_Deti(){return IN_Perevod_Deti.get();}
	public void setIN_Perevod_Deti(int value){this.IN_Perevod_Deti.set(value);}
	
	private SimpleIntegerProperty IN_Perevod_Mother = new SimpleIntegerProperty();
	public int getIN_Perevod_Mother(){return IN_Perevod_Mother.get();}
	public void setIN_Perevod_Mother(int value){this.IN_Perevod_Mother.set(value);}
	
	private SimpleIntegerProperty OUT_Perevod = new SimpleIntegerProperty();
	public int getOUT_Perevod(){return OUT_Perevod.get();}
	public void setOUT_Perevod(int value){this.OUT_Perevod.set(value);}
	
	private SimpleIntegerProperty OUT_Perevod_Deti = new SimpleIntegerProperty();
	public int getOUT_Perevod_Deti(){return OUT_Perevod_Deti.get();}
	public void setOUT_Perevod_Deti(int value){this.OUT_Perevod_Deti.set(value);}
	
	private SimpleIntegerProperty OUT_Perevod_Mother = new SimpleIntegerProperty();
	public int getOUT_Perevod_Mother(){return OUT_Perevod_Mother.get();}
	public void setOUT_Perevod_Mother(int value){this.OUT_Perevod_Mother.set(value);}
	
	private SimpleIntegerProperty IN_Deti = new SimpleIntegerProperty();
	public int getIN_Deti(){return IN_Deti.get();}
	public void setIN_Deti(int value){this.IN_Deti.set(value);}
	
	private SimpleIntegerProperty OUT_Deti = new SimpleIntegerProperty();
	public int getOUT_Deti(){return OUT_Deti.get();}
	public void setOUT_Deti(int value){this.OUT_Deti.set(value);}
	
	private SimpleIntegerProperty IN_Mother = new SimpleIntegerProperty();
	public int getIN_Mother(){return IN_Mother.get();}
	public void setIN_Mother(int value){this.IN_Mother.set(value);}
	
	private SimpleIntegerProperty OUT_Mother = new SimpleIntegerProperty();
	public int getOUT_Mother(){return OUT_Mother.get();}
	public void setOUT_Mother(int value){this.OUT_Mother.set(value);}
	
	private SimpleIntegerProperty DIE = new SimpleIntegerProperty();
	public int getDIE(){return DIE.get();}
	public void setDIE(int value){this.DIE.set(value);}
	
	private SimpleIntegerProperty DIE_Deti = new SimpleIntegerProperty();
	public int getDIE_Deti(){return DIE_Deti.get();}
	public void setDIE_Deti(int value){this.DIE_Deti.set(value);}
	
	private SimpleStringProperty Date = new SimpleStringProperty();
	public String getDate(){return Date.get();}
	public void setDate(String value){this.Date.set(value);}
	
	private SimpleIntegerProperty id_OK = new SimpleIntegerProperty();
	public int getid_OK(){return id_OK.get();}
	public void setid_OK(int value){this.id_OK.set(value);}
	
	private SimpleIntegerProperty id_Osnova = new SimpleIntegerProperty();
	public int getid_Osnova(){return id_Osnova.get();}
	public void setid_Osnova(int value){this.id_Osnova.set(value);}

}
