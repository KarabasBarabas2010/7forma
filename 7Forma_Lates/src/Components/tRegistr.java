package Components;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class tRegistr {
	
	public tRegistr(){
		this.Flag = new SimpleBooleanProperty(false);
	}
	
	private SimpleStringProperty Name = new SimpleStringProperty();
	public String getName(){return Name.get();}
	public void setName(String value){this.Name.set(value);}
	
	private SimpleIntegerProperty Chislo_Koek = new SimpleIntegerProperty();
	public int getChislo_Koek(){return Chislo_Koek.get();}
	public void setChislo_Koek(int value){this.Chislo_Koek.set(value);}
	
	private SimpleIntegerProperty Nachalo = new SimpleIntegerProperty();
	public int getNachalo(){return Nachalo.get();}
	public void setNachalo(int value){this.Nachalo.set(value);}
	
	private SimpleIntegerProperty Nachalo_Deti = new SimpleIntegerProperty();
	public int getNachalo_Deti(){return Nachalo_Deti.get();}
	public void setNachalo_Deti(int value){this.Nachalo_Deti.set(value);}
	
	private SimpleIntegerProperty Nachalo_Mother = new SimpleIntegerProperty();
	public int getNachalo_Mother(){return Nachalo_Mother.get();}
	public void setNachalo_Mother(int value){this.Nachalo_Mother.set(value);}
	
	private SimpleIntegerProperty Vsego = new SimpleIntegerProperty();
	public int getVsego(){return Vsego.get();}
	public void setVsego(int value){this.Vsego.set(value);}
	
	private SimpleIntegerProperty Vsego_Deti = new SimpleIntegerProperty();
	public int getVsego_Deti(){return Vsego_Deti.get();}
	public void setVsego_Deti(int value){this.Vsego_Deti.set(value);}
	
	private SimpleIntegerProperty Vsego_Selo = new SimpleIntegerProperty();
	public int getVsego_Selo(){return Vsego_Selo.get();}
	public void setVsego_Selo(int value){this.Vsego_Selo.set(value);}
	
	private SimpleIntegerProperty Vsego_Mother = new SimpleIntegerProperty();
	public int getVsego_Mother(){return Vsego_Mother.get();}
	public void setVsego_Mother(int value){this.Vsego_Mother.set(value);}
	
	private SimpleStringProperty Date = new SimpleStringProperty();
	public String getDate(){return Date.get();}
	public void setDate(String value){this.Date.set(value);}
	
	private final BooleanProperty Flag;
	public void setFlag(boolean val) {this.Flag.setValue(val);}
	public boolean getFlag() {return Flag.get();}
	
	private SimpleIntegerProperty id_OK = new SimpleIntegerProperty();
	public int getid_OK(){return id_OK.get();}
	public void setid_OK(int value){this.id_OK.set(value);}
	
	private final BooleanProperty highlightProperty = new SimpleBooleanProperty( false );
	public void setHighlight( boolean value ) { highlightProperty.set( value );}
	public boolean isHighlight() { return highlightProperty.get(); }
	
}
