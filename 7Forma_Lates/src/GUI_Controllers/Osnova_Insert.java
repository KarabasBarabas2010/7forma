package GUI_Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.Logger;
import org.jfree.util.Log;

import com.sun.javafx.scene.traversal.Direction;

import ConnectManager.LoggerAllAplication;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

@SuppressWarnings("restriction")
public class Osnova_Insert implements Initializable{
	private static Logger log = LoggerAllAplication.getlogger();
	private Osnova Parent;
	private Stage thisdialogstage;
	private Integer id_Otdelenie;
	private Integer id_OK;
	private Integer id_Osnova;
	private String OK_Date;
	private Integer update_IN_ALL;
	private Integer update_IN_Selo;
	private Integer update_OUT_All;
	private Integer update_OUT_Selo;
	private Integer update_OUT_DrSt;
	private Integer update_IN_Perevod;
	private Integer update_IN_Perevod_Deti;
	private Integer update_IN_Perevod_Mother;
	private Integer update_OUT_Perevod;
	private Integer update_OUT_Perevod_Deti;
	private Integer update_OUT_Perevod_Mother;
	private Integer update_IN_Deti;
	private Integer update_OUT_Deti;
	private Integer update_IN_Mother;
	private Integer update_OUT_Mother;
	private Integer update_DIE;
	private Integer update_DIE_Deti;
	private String Date; 
	private Integer r_Vsego;
	private Integer r_Vsego_Selo;
	private Integer r_Vsego_Deti;
	private Integer r_Vsego_Mother;
	private Integer r_id_Registr;
	private boolean TypeFormi;
	//Если Истина то Добавить иначе Изменить
	
	@FXML ComboBox<String> ViborKoiki = new ComboBox<String>();
	@FXML private Label ViborKoiki1; 
	@FXML private Button btnInsert;
	@FXML private TextField IN_All; 
	@FXML private TextField IN_Selo; 
	@FXML private TextField IN_Deti; 
	@FXML private TextField OUT_All; 
	@FXML private TextField OUT_Selo;
	@FXML private TextField OUT_DrSt;
	@FXML private TextField OUT_Deti; 
	@FXML private TextField IN_Perevod;
	@FXML private TextField IN_Perevod_Deti;
	@FXML private TextField IN_Perevod_Mother;
	@FXML private TextField OUT_Perevod; 
	@FXML private TextField OUT_Perevod_Deti; 
	@FXML private TextField OUT_Perevod_Mother;
	@FXML private TextField IN_Mother;
	@FXML private TextField OUT_Mother;
	@FXML private TextField DIE; 
	@FXML private TextField DIE_Deti; 

	Connection conn = ConnectManager.SQLiteJDBC.Get_Conn();	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}
	
	public void InsertTextInLabel(String text){
		ViborKoiki1.setText(text);
	}
	
	@FXML public void onInsert(ActionEvent event) throws Exception {
		try{
			if (!ProverkaZapolneniyaPoleiy()){
				if(isTypeFormi()){
					insert();			
				} else{
					update();
				}			
			}			
		}catch(NumberFormatException e){
			JOptionPane.showMessageDialog(null, "Непроавильно заполнено поле", "Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
		}catch(SQLException e){
			JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка в запросе", JOptionPane.ERROR_MESSAGE);
		}
				
	}
	
	private void insert() throws SQLException,NumberFormatException{
		String sql = "SELECT r.id_Registr AS id_Registr,r.Vsego AS Vsego,r.Vsego_Deti AS Vsego_Deti,"
				+ "r.Vsego_Selo AS Vsego_Selo,r.Vsego_Mother AS Vsego_Mother FROM Registr r INNER JOIN OK ok ON "
				+ "r.id_Registr = ok.id_Registr WHERE ok.id_OK = "+ getId_OK();
		
		ResultSet rs = conn.createStatement().executeQuery(sql);
		while(rs.next()){
			setR_id_Registr(rs.getInt("id_Registr"));
			setR_Vsego(rs.getInt("Vsego"));
			setR_Vsego_Deti(rs.getInt("Vsego_Deti"));
			setR_Vsego_Selo(rs.getInt("Vsego_Selo"));
			setR_Vsego_Mother(rs.getInt("Vsego_Mother"));
		}
		rs.close();
		
		int Vsego = Integer.valueOf(getR_Vsego())+Integer.valueOf(IN_All.getText())-Integer.valueOf(OUT_All.getText())-
				Integer.valueOf(DIE.getText())+Integer.valueOf(IN_Perevod.getText())-Integer.valueOf(OUT_Perevod.getText());
		int Vsego_Deti =Integer.valueOf(getR_Vsego_Deti())+Integer.valueOf(IN_Deti.getText())-Integer.valueOf(OUT_Deti.getText())-
				Integer.valueOf(DIE_Deti.getText())+Integer.valueOf(IN_Perevod_Deti.getText())-Integer.valueOf(OUT_Perevod_Deti.getText());
		int Vsego_Selo =Integer.valueOf(getR_Vsego_Selo())+Integer.valueOf(IN_Selo.getText())-Integer.valueOf(OUT_Selo.getText());
		int Vsego_Mother = Integer.valueOf(getR_Vsego_Mother())+Integer.valueOf(IN_Mother.getText())-Integer.valueOf(OUT_Mother.getText())+
				Integer.valueOf(IN_Perevod_Mother.getText())-Integer.valueOf(OUT_Perevod_Mother.getText());
			
		conn.setAutoCommit(false);
		String query = "INSERT INTO Osnova (id_OK,IN_All,OUT_All,IN_Selo,OUT_Selo,OUT_DrSt,IN_Perevod,OUT_Perevod,IN_Deti,OUT_Deti,"
				+ "IN_Mother,OUT_Mother,DIE,IN_Perevod_Deti,OUT_Perevod_Deti,DIE_Deti,Date,IN_Perevod_Mother,OUT_Perevod_Mother) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt = conn.prepareStatement(query);
		log.debug(query);
		//Штыуке
		pstmt.setInt(1,getId_OK());
		pstmt.setString(2,IN_All.getText());
		pstmt.setString(3,OUT_All.getText());		
		pstmt.setString(4,IN_Selo.getText());
		pstmt.setString(5,OUT_Selo.getText());
		pstmt.setString(6,OUT_DrSt.getText());
		pstmt.setString(7,IN_Perevod.getText());
		pstmt.setString(8,OUT_Perevod.getText());
		pstmt.setString(9,IN_Deti.getText());
		pstmt.setString(10,OUT_Deti.getText());
		pstmt.setString(11,IN_Mother.getText());
		pstmt.setString(12,OUT_Mother.getText());
		pstmt.setString(13,DIE.getText());
		pstmt.setString(14,IN_Perevod_Deti.getText());
		pstmt.setString(15,OUT_Perevod_Deti.getText());
		pstmt.setString(16,DIE_Deti.getText());
		pstmt.setString(17,getDate());
		pstmt.setString(18,IN_Perevod_Mother.getText());
		pstmt.setString(19,OUT_Perevod_Mother.getText());

		log.debug(getId_OK());
		pstmt.executeUpdate();
				
		String query2 ="UPDATE Registr SET "
				+ "Vsego = "+Vsego
				+ ",Vsego_Deti = "+Vsego_Deti
				+ ",Vsego_Selo = "+Vsego_Selo
				+ ",Vsego_Mother = "+Vsego_Mother
				+ ",Date = '"+LocalDate.now().toString()+"'"
				+ " WHERE (id_Registr = "+getR_id_Registr()+")";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(query2);		
		
		conn.commit();
		pstmt.close();
		stmt.close();
		conn.setAutoCommit(true);
		getDialogstage().close();
		getParent().retry();
	}

	private void update() throws SQLException,NumberFormatException{
		String sql = "SELECT r.id_Registr AS id_Registr,r.Vsego AS Vsego,r.Vsego_Deti AS Vsego_Deti,"
				+ "r.Vsego_Selo AS Vsego_Selo,r.Vsego_Mother AS Vsego_Mother FROM Registr r INNER JOIN OK ok ON "
				+ "r.id_Registr = ok.id_Registr WHERE ok.id_OK = "+ getId_OK();
		
		ResultSet rs = conn.createStatement().executeQuery(sql);
		while(rs.next()){
			setR_id_Registr(rs.getInt("id_Registr"));
			setR_Vsego(rs.getInt("Vsego"));
			setR_Vsego_Deti(rs.getInt("Vsego_Deti"));
			setR_Vsego_Selo(rs.getInt("Vsego_Selo"));
			setR_Vsego_Mother(rs.getInt("Vsego_Mother"));
		}
		rs.close();
		conn.setAutoCommit(false);
		String query = "UPDATE Osnova SET id_OK = ?,IN_All = ?,OUT_All = ?,IN_Selo = ?,OUT_Selo = ?,OUT_DrSt = ?,"
				+ "IN_Deti = ?,OUT_Deti = ?,IN_Mother = ?,OUT_Mother = ?,IN_Perevod = ?,OUT_Perevod = ?,DIE = ?,"
				+ "IN_Perevod_Deti = ?,OUT_Perevod_Deti = ?,DIE_Deti = ?,IN_Perevod_Mother = ?,OUT_Perevod_Mother = ?,Date = ? " +
                "WHERE id_Osnova = "+getId_Osnova();
		PreparedStatement pstmt = conn.prepareStatement(query);
		pstmt.setInt(1,getId_OK());
		pstmt.setInt(2,Integer.parseInt(IN_All.getText()));
		pstmt.setInt(3,Integer.parseInt(OUT_All.getText()));
		pstmt.setInt(4,Integer.parseInt(IN_Selo.getText()));
		pstmt.setInt(5,Integer.parseInt(OUT_Selo.getText()));
		pstmt.setInt(6,Integer.parseInt(OUT_DrSt.getText()));
		pstmt.setInt(7,Integer.parseInt(IN_Deti.getText()));
		pstmt.setInt(8,Integer.parseInt(OUT_Deti.getText()));
		pstmt.setInt(9,Integer.parseInt(IN_Mother.getText()));
		pstmt.setInt(10,Integer.parseInt(OUT_Mother.getText()));
		pstmt.setInt(11,Integer.parseInt(IN_Perevod.getText()));
		pstmt.setInt(12,Integer.parseInt(OUT_Perevod.getText()));
		pstmt.setInt(13,Integer.parseInt(DIE.getText()));
		pstmt.setInt(14,Integer.parseInt(IN_Perevod_Deti.getText()));
		pstmt.setInt(15,Integer.parseInt(OUT_Perevod_Deti.getText()));
		pstmt.setInt(16,Integer.parseInt(DIE_Deti.getText()));
		pstmt.setInt(17,Integer.parseInt(IN_Perevod_Mother.getText()));
		pstmt.setInt(18,Integer.parseInt(OUT_Perevod_Mother.getText()));
		
		pstmt.setString(19,getDate());
		
		pstmt.executeUpdate();
		
		int Vsego = getR_Vsego()-(update_IN_ALL-update_OUT_All-update_DIE+update_IN_Perevod-update_OUT_Perevod
				-Integer.valueOf(IN_All.getText())+Integer.valueOf(OUT_All.getText())+Integer.valueOf(DIE.getText())
				-Integer.valueOf(IN_Perevod.getText())+Integer.valueOf(OUT_Perevod.getText()));
		int Vsego_Deti = getR_Vsego_Deti()-(update_IN_Deti-update_OUT_Deti-update_DIE_Deti+update_IN_Perevod_Deti-update_OUT_Perevod_Deti
				-Integer.valueOf(IN_Deti.getText())+Integer.valueOf(OUT_Deti.getText())+Integer.valueOf(DIE_Deti.getText())
				-Integer.valueOf(IN_Perevod_Deti.getText())+Integer.valueOf(OUT_Perevod_Deti.getText()));
		int Vsego_Selo = getR_Vsego_Selo()-(update_IN_Selo-update_OUT_Selo-Integer.valueOf(IN_Selo.getText())
				+Integer.valueOf(OUT_Selo.getText()));
		int Vsego_Mother = getR_Vsego_Mother()-(update_IN_Mother-update_OUT_Mother+update_IN_Perevod_Mother-update_OUT_Perevod_Mother
				-Integer.valueOf(IN_Mother.getText())+Integer.valueOf(OUT_Mother.getText())
				-Integer.valueOf(IN_Perevod_Mother.getText())+Integer.valueOf(OUT_Perevod_Mother.getText()));
				
		String query2 ="UPDATE Registr SET Vsego = "+Vsego				
						+ ",Vsego_Deti = "+Vsego_Deti
						+ ",Vsego_Selo = "+Vsego_Selo
						+ ",Vsego_Mother = "+Vsego_Mother
						+ ",Date = '"+LocalDate.now().toString()+"'"
				+ " WHERE (id_Registr = "+getR_id_Registr()+")";
		

		Statement stmt = conn.createStatement();
		stmt.executeUpdate(query2);

		conn.commit();
		conn.setAutoCommit(true);
		pstmt.close();
		stmt.close();
		getDialogstage().close();
		getParent().retry();		
	}
	
	private boolean ProverkaZapolneniyaPoleiy() {
		if(IN_All.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Поступило всего'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(IN_Deti.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Поступило детей до года'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(IN_Selo.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Поступило в т.ч. сельских'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(OUT_All.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Выписано всего'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(OUT_Deti.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Выписано детей до года'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(OUT_Selo.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Выписано в т.ч. сельских'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(IN_Mother.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Поступило матерей по уходу'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(OUT_Mother.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Выписано матерей по уходу'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(IN_Perevod.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Переведено из других отделений'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(OUT_Perevod.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Переведено в другие отделения'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(DIE.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Умерло'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(OUT_DrSt.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'В т.ч. в др стационар'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(OUT_Perevod_Mother.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Перевод по матерям'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		if(IN_Perevod_Mother.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Не заполнено поле \n'Перевод по матерям'","Ошибка заполнения", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	@FXML public void KeyPress(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){
			getDialogstage().close();
		}
		if (key.getCode() == KeyCode.UP){
			//thisdialogstage.getScene().getFocusOwner()..impl_traverse(Direction.UP);
		}
		if (key.getCode() == KeyCode.DOWN){
			//thisdialogstage.getScene().getFocusOwner().impl_traverse(Direction.DOWN);
		}
	}
	
	@FXML public void retry(){
		//("smena focusa");

	}
	
	public Stage getDialogstage() {
		return thisdialogstage;
	}
	public void setDialogstage(Stage dialogstage) {
		this.thisdialogstage = dialogstage;
	}
	public Integer getId_Otdelenie() {
		return id_Otdelenie;
	}
	public void setId_Otdelenie(Integer id_otd) throws SQLException {
		this.id_Otdelenie = id_otd;
//		ViborKoiki.setItems(FXCollections.observableArrayList(Get_List()));
	}
	public Integer getId_OK() {
		return id_OK;
	}
	public void setId_OK(Integer integer) {
		this.id_OK = integer;
	}
	public Integer getId_Osnova() {
		return id_Osnova;
	}
	public void setId_Osnova(Integer id_Osnova) {
		this.id_Osnova = id_Osnova;
	}
	public String getOK_Date() {
		return OK_Date;
	}
	public void setOK_Date(String oK_Date) {
		OK_Date = oK_Date;
	}
	public boolean isTypeFormi() {
		return TypeFormi;
	}
	public void setTypeFormi(boolean typeFormi) {
		this.TypeFormi = typeFormi;
		if(typeFormi){
			btnInsert.setText("Добавить");
		}else{
			btnInsert.setText("Изменить");
		}
	}
	public Integer getUpdate_IN_ALL() {
		return update_IN_ALL;
	}
	public void setUpdate_IN_ALL(Integer update_IN_ALL) {
		this.IN_All.setText(update_IN_ALL.toString());
		this.update_IN_ALL = update_IN_ALL;
	}
	public Integer getUpdate_IN_Selo() {
		return update_IN_Selo;
	}
	public void setUpdate_IN_Selo(Integer update_IN_Selo) {
		this.IN_Selo.setText(update_IN_Selo.toString());
		this.update_IN_Selo = update_IN_Selo;
	}
	public Integer getUpdate_OUT_All() {
		return update_OUT_All;
	}
	public void setUpdate_OUT_All(Integer update_OUT_All) {
		this.OUT_All.setText(update_OUT_All.toString());
		this.update_OUT_All = update_OUT_All;
	}
	public Integer getUpdate_OUT_Selo() {
		return update_OUT_Selo;
	}
	public void setUpdate_OUT_Selo(Integer update_OUT_Selo) {
		this.OUT_Selo.setText(update_OUT_Selo.toString());
		this.update_OUT_Selo = update_OUT_Selo;
	}
	public Integer getUpdate_OUT_DrSt() {
		return update_OUT_DrSt;
	}
	public void setUpdate_OUT_DrSt(Integer update_OUT_DrSt) {
		this.OUT_DrSt.setText(update_OUT_DrSt.toString());
		this.update_OUT_Selo = update_OUT_DrSt;
	}
	public Integer getUpdate_IN_Perevod() {
		return update_IN_Perevod;
	}
	public void setUpdate_IN_Perevod(Integer update_IN_Perevod) {
		this.IN_Perevod.setText(update_IN_Perevod.toString());
		this.update_IN_Perevod = update_IN_Perevod;
	}
	public Integer getUpdate_IN_Perevod_Deti() {
		return update_IN_Perevod_Deti;
	}
	public void setUpdate_IN_Perevod_Deti(Integer update_IN_Perevod_Deti) {
		this.IN_Perevod_Deti.setText(update_IN_Perevod_Deti.toString());
		this.update_IN_Perevod_Deti = update_IN_Perevod_Deti;
	}
	public Integer getUpdate_IN_Perevod_Mother() {
		return update_IN_Perevod_Mother;
	}
	public void setUpdate_IN_Perevod_Mother(Integer update_IN_Perevod_Mother) {
		this.IN_Perevod_Mother.setText(update_IN_Perevod_Mother.toString());
		this.update_IN_Perevod_Mother = update_IN_Perevod_Mother;
	}
	public Integer getUpdate_OUT_Perevod() {
		return update_OUT_Perevod;
	}
	public void setUpdate_OUT_Perevod(Integer update_OUT_Perevod) {
		this.OUT_Perevod.setText(update_OUT_Perevod.toString());
		this.update_OUT_Perevod = update_OUT_Perevod;
	}
	public Integer getUpdate_OUT_Perevod_Deti() {
		return update_OUT_Perevod_Deti;
	}
	public void setUpdate_OUT_Perevod_Deti(Integer update_OUT_Perevod_Deti) {
		this.OUT_Perevod_Deti.setText(update_OUT_Perevod_Deti.toString());
		this.update_OUT_Perevod_Deti = update_OUT_Perevod_Deti;
	}
	public Integer getUpdate_OUT_Perevod_Mother() {
		return update_OUT_Perevod_Mother;
	}
	public void setUpdate_OUT_Perevod_Mother(Integer update_OUT_Perevod_Mother) {
		this.OUT_Perevod_Mother.setText(update_OUT_Perevod_Mother.toString());
		this.update_OUT_Perevod_Mother = update_OUT_Perevod_Mother;
	}
	public Integer getUpdate_IN_Deti() {
		return update_IN_Deti;
	}
	public void setUpdate_IN_Deti(Integer update_IN_Deti) {
		this.IN_Deti.setText(update_IN_Deti.toString());
		this.update_IN_Deti = update_IN_Deti;
	}
	public Integer getUpdate_OUT_Deti() {
		return update_OUT_Deti;
	}
	public void setUpdate_OUT_Deti(Integer update_OUT_Deti) {
		this.OUT_Deti.setText(update_OUT_Deti.toString());
		this.update_OUT_Deti = update_OUT_Deti;
	}
	public Integer getUpdate_IN_Mother() {return update_IN_Mother;	}
	public void setUpdate_IN_Mother(Integer update_IN_Mother) {
		this.IN_Mother.setText(update_IN_Mother.toString());
		this.update_IN_Mother = update_IN_Mother;
	}
	public Integer getUpdate_OUT_Mother() {
		return update_OUT_Mother;
	}
	public void setUpdate_OUT_Mother(Integer update_OUT_Mother) {
		this.OUT_Mother.setText(update_OUT_Mother.toString());
		this.update_OUT_Mother = update_OUT_Mother;
	}
	public Integer getUpdate_DIE() {
		return update_DIE;
	}
	public void setUpdate_DIE(Integer update_DIE) {
		this.DIE.setText(update_DIE.toString());
		this.update_DIE = update_DIE;
	}
	public Integer getUpdate_DIE_Deti() {
		return update_DIE_Deti;
	}
	public void setUpdate_DIE_Deti(Integer update_DIE_Deti) {
		this.DIE_Deti.setText(update_DIE_Deti.toString());
		this.update_DIE_Deti = update_DIE_Deti;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public Integer getR_Vsego() {
		return (r_Vsego == null ? 0 : r_Vsego);
	}
	public void setR_Vsego(Integer r_Vsego) {
		this.r_Vsego = r_Vsego;
	}
	public Integer getR_Vsego_Selo() {
		return (r_Vsego_Selo == null ? 0 : r_Vsego_Selo);
	}
	public void setR_Vsego_Selo(Integer r_Vsego_Selo) {
		this.r_Vsego_Selo = r_Vsego_Selo;
	}
	public Integer getR_Vsego_Deti() {
		return (r_Vsego_Deti == null ? 0 : r_Vsego_Deti);
	}
	public void setR_Vsego_Deti(Integer r_Vsego_Deti) {
		this.r_Vsego_Deti = r_Vsego_Deti;
	}
	public Integer getR_Vsego_Mother() {
		return (r_Vsego_Mother == null ? 0 : r_Vsego_Mother);
	}
	public void setR_Vsego_Mother(Integer r_Vsego_Mother) {
		this.r_Vsego_Mother = r_Vsego_Mother;
	}
	public Integer getR_id_Registr() {
		return r_id_Registr;
	}
	public void setR_id_Registr(Integer r_id_Registr) {
		this.r_id_Registr = r_id_Registr;
	}
	
	@FXML public void ComboAction(){
//		setId_OK(dateid_OK.get(ViborKoiki.getSelectionModel().getSelectedIndex()));
//		btnInsert.setDisable(false);
		
		// id_OK = SQLiteJDBC.getSecondValue().get(ViborKoiki.getSelectionModel().getSelectedIndex())
		
		//System.out.println("Osnova_Insert ComboAction: "+SQLiteJDBC.getSecondValue().get(ViborKoiki.getSelectionModel().getSelectedIndex()));
		//ViewTable(str);
		//Table_Otdelenie.getColumns().get(2).setVisible(false);
		//btnAdd.setDisable(false);
	}

	public void setParent(Osnova osnova) {
		this.Parent=osnova;
	}	
	public Osnova getParent(){
		return Parent;
	}

}
