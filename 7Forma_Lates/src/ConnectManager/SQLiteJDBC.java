package ConnectManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.Logger;

import GUI_Controllers.Osnova;
import application.Props;

public class SQLiteJDBC {
	private static Logger log = LoggerAllAplication.getlogger();

	public static Connection Get_Conn(){
		try {
			Class.forName(Props.JDBC_BD);
			return DriverManager.getConnection(Props.CONNECT_BD+Props.PATH_TO_BD);
			//return DriverManager.getConnection("jdbc:sqlite:BD.sqlite");
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage() );
			log.debug(e);
			return null;
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage() );
			log.debug(e);
			return null;
		}
	}

}
