package GUI_Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.Logger;

import Components.tRegistr;
import Components.tnu_ochen_nujna9;
import ConnectManager.LoggerAllAplication;
import ConnectManager.SQLiteJDBC;
//import groovy.lang.Singleton;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.print.PrinterJob;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.scene.control.TableRow;

public class Osnova implements Initializable {
	
	private static Logger log = LoggerAllAplication.getlogger();
	
	private Stage thisdialogstage;
	Connection conn = SQLiteJDBC.Get_Conn();

	@FXML
	private DatePicker calendar;
	@FXML
	public TableView<Components.tRegistr> table = new TableView<Components.tRegistr>();
	@FXML
	public TableView<Components.tOsnova> tab2 = new TableView<Components.tOsnova>();
	@FXML
	public TableView<Components.tnu_ochen_nujna9> tab_nu_ochen_nujna9 = new TableView<Components.tnu_ochen_nujna9>();
	@FXML ComboBox<String> ViborOtdeleni9 = new ComboBox<String>();

	private ObservableList<Components.tRegistr> masterData = FXCollections.observableArrayList();
	private ObservableList<Components.tOsnova> masterDataTab2 = FXCollections.observableArrayList();
	private ObservableList<Components.tnu_ochen_nujna9> masterDataTab_nu_ochen_nujna9 = FXCollections.observableArrayList();
	private ObservableList<Integer> dataidotdelenia = FXCollections.observableArrayList();
	private ObservableList<Integer> dataidosnova = FXCollections.observableArrayList();
	private int dataidok;

	@FXML
	public void onOpen_Otdelenie(ActionEvent event) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/Otdelenie.fxml"));
			Parent root = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setTitle("Справочник отделений");
			stage.setScene(new Scene(root));
			Otdelenie controller = (Otdelenie) loader.getController();
			controller.setDialogstage(stage);
			stage.showAndWait();
		} catch (IOException e) {
			log.error(e);
		}

	}
	@FXML
	public void onOpen_Koiki(ActionEvent event) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/Koiki.fxml"));
			Parent root = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			// stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Справочник коек");
			stage.setScene(new Scene(root));
			Koiki controller = (Koiki) loader.getController();
			controller.setDialogstage(stage);
			stage.showAndWait();
		} catch (IOException e) {
			log.error(e);
		}

	}
	@FXML
	public void onOpen_OK(ActionEvent event) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/OK.fxml"));
			Parent root = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setTitle("Плановые показатели");
			stage.setScene(new Scene(root));
			OK controller = (OK) loader.getController();
			controller.setDialogstage(stage);
			stage.showAndWait();
		} catch (IOException e) {
			log.error(e);
		}

	}
	@FXML
	public void onOpen_OP(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/OP.fxml"));
		Parent root = (Parent) loader.load();
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setTitle("Плановые показатели");
		stage.setScene(new Scene(root));
		OP controller = (OP) loader.getController();
		controller.setDialogstage(stage);
		stage.showAndWait();
	}
	@FXML
	public void onOpen_OP_Otdelenie(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/OP_Otdelenie.fxml"));
		Parent root = (Parent) loader.load();
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setTitle("Плановые показатели по отделениям");
		stage.setScene(new Scene(root));
		OP_Otdelenie controller = (OP_Otdelenie) loader.getController();
		controller.setDialogstage(stage);
		stage.showAndWait();
	}
	@FXML
	public void onOpen_SV(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/SV.fxml"));
		Parent root = (Parent) loader.load();
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setTitle("Сводная ведомость");
		stage.setScene(new Scene(root));
		SV controller = (SV) loader.getController();
		controller.setDialogstage(stage);
		stage.showAndWait();
	}
	@FXML
	public void onOpen_SV_Otdelenie(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/SV_Otdelenie.fxml"));
		Parent root = (Parent) loader.load();
		Stage stage = new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setTitle("Сводная ведомость по отделениям");
		stage.setScene(new Scene(root));
		SV_Otdelenie controller = (SV_Otdelenie) loader.getController();
		controller.setDialogstage(stage);
		stage.showAndWait();
	}
	@FXML
	public void onQuite(ActionEvent event) throws Exception {
		Platform.exit();
		System.exit(0);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		CreateTable();
		CreateTable2();
		CreateTab_nu_ochen_nujna9();
		// table.getColumns().clear();
		ViborOtdeleni9.setItems(FXCollections.observableArrayList(Get_List()));
		ViborOtdeleni9.getSelectionModel().select(0);
		calendar.setValue(LocalDate.now());
		InitializeMenuItemToButton();
		ViewTable(dataidotdelenia.get(ViborOtdeleni9.getSelectionModel().getSelectedIndex()), calendar.getValue());
	}

	@SuppressWarnings("unchecked")
	private void CreateTable() {
		table.getItems().clear();
		masterData.clear();
		dataidosnova.clear();

		TableColumn<Components.tRegistr, String> Koika = new TableColumn<Components.tRegistr, String>("Койка");
		Koika.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, String>("Name"));

		TableColumn<Components.tRegistr, String> Sostoit_na_Nachalo = new TableColumn<Components.tRegistr, String>(
				"Состоит на начало суток");
		TableColumn<Components.tRegistr, String> Sostoit = new TableColumn<Components.tRegistr, String>(
				"Состоит всего");

		TableColumn<Components.tRegistr, Integer> Nachalo = new TableColumn<Components.tRegistr, Integer>("Всего");
		Nachalo.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Integer>("Nachalo"));
		Nachalo.setCellFactory(new Callback<TableColumn<Components.tRegistr, Integer>, TableCell<Components.tRegistr, Integer>>() {
			@Override
			public TableCell<tRegistr, Integer> call(TableColumn<tRegistr, Integer> param) {
				// TODO Auto-generated method stub
				return new StyleValueCell();
			}});

		TableColumn<Components.tRegistr, Integer> Nachalo_Deti = new TableColumn<Components.tRegistr, Integer>(
				"Дети до года");
		Nachalo_Deti.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Integer>("Nachalo_Deti"));
		TableColumn<Components.tRegistr, Integer> Nachalo_Mother = new TableColumn<Components.tRegistr, Integer>(
				"Матери по уходу");
		Nachalo_Mother.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Integer>("Nachalo_Mother"));

		TableColumn<Components.tRegistr, Integer> Chislo_Koek = new TableColumn<Components.tRegistr, Integer>(
				"Число коек");
		Chislo_Koek.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Integer>("Chislo_Koek"));
//		Chislo_Koek.setCellFactory(new Callback<TableColumn<Components.tRegistr, Integer>, TableCell<Components.tRegistr, Integer>>() {
//			@Override
//			public TableCell<tRegistr, Integer> call(TableColumn<tRegistr, Integer> param) {
//				// TODO Auto-generated method stub
//				return new StyleValueCell();
//			}});
		TableColumn<Components.tRegistr, Integer> Vsego = new TableColumn<Components.tRegistr, Integer>("Всего");
		Vsego.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Integer>("Vsego"));
		Vsego.setCellFactory(new Callback<TableColumn<Components.tRegistr, Integer>, TableCell<Components.tRegistr, Integer>>() {
			@Override
			public TableCell<tRegistr, Integer> call(TableColumn<tRegistr, Integer> param) {
				// TODO Auto-generated method stub
				return new StyleValueCell();
			}});
		TableColumn<Components.tRegistr, Integer> Vsego_Selo = new TableColumn<Components.tRegistr, Integer>(
				"в т.ч. Село");
		Vsego_Selo.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Integer>("Vsego_Selo"));
		TableColumn<Components.tRegistr, Integer> Vsego_Deti = new TableColumn<Components.tRegistr, Integer>(
				"Дети до года");
		Vsego_Deti.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Integer>("Vsego_Deti"));
		TableColumn<Components.tRegistr, Integer> Vsego_Mother = new TableColumn<Components.tRegistr, Integer>(
				"Матери по уходу");
		Vsego_Mother.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Integer>("Vsego_Mother"));

		TableColumn<Components.tRegistr, Boolean> flag = new TableColumn<Components.tRegistr, Boolean>();
		flag.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Boolean>("Flag"));
		flag.setVisible(false);

		TableColumn<Components.tRegistr, Integer> id_OK = new TableColumn<Components.tRegistr, Integer>("id_OK");
		id_OK.setCellValueFactory(new PropertyValueFactory<Components.tRegistr, Integer>("id_OK"));
		id_OK.setVisible(false);

		Sostoit_na_Nachalo.getColumns().addAll(Nachalo, Nachalo_Deti, Nachalo_Mother);
		Sostoit.getColumns().addAll(Vsego, Vsego_Deti, Vsego_Mother, Vsego_Selo);

//		Nachalo.setStyle("-fx-border-style: solid;");
//		Nachalo_Mother.setStyle("-fx-border-style: solid;");

		table.getColumns().setAll(Koika, Chislo_Koek, Sostoit_na_Nachalo, Sostoit);
		for(int index=0;index<table.getColumns().size();index++){
			for(int j=0;j<table.getColumns().get(index).getColumns().size();j++){
				table.getColumns().get(index).getColumns().get(j).setSortable(false);
				table.getColumns().get(index).getColumns().get(j).setStyle("-fx-alignment: center;");
			}
			table.getColumns().get(index).setSortable(false);
		}	

	}
	@SuppressWarnings("unchecked")
	private void CreateTable2() {
		tab2.getItems().clear();
		masterDataTab2.clear();
		TableColumn<Components.tOsnova, String> Postupilo = new TableColumn<Components.tOsnova, String>("Поступило");
		TableColumn<Components.tOsnova, String> Vibilo = new TableColumn<Components.tOsnova, String>("Выбыло");
		TableColumn<Components.tOsnova, String> Perevod = new TableColumn<Components.tOsnova, String>("Переводы");
		TableColumn<Components.tOsnova, Integer> OUT_DrSt = new TableColumn<Components.tOsnova, Integer>(
				"др. Стационар");
		OUT_DrSt.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("OUT_DrSt"));
		TableColumn<Components.tOsnova, String> Deti = new TableColumn<Components.tOsnova, String>("Дети до года");
		TableColumn<Components.tOsnova, String> Mother = new TableColumn<Components.tOsnova, String>("Матери");

		TableColumn<Components.tOsnova, Integer> IN_All = new TableColumn<Components.tOsnova, Integer>("Всего");
		IN_All.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("IN_All"));
		TableColumn<Components.tOsnova, Integer> IN_Selo = new TableColumn<Components.tOsnova, Integer>("в т.ч. Село");
		IN_Selo.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("IN_Selo"));
		TableColumn<Components.tOsnova, Integer> OUT_All = new TableColumn<Components.tOsnova, Integer>("Всего");
		OUT_All.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("OUT_All"));
		TableColumn<Components.tOsnova, Integer> OUT_Selo = new TableColumn<Components.tOsnova, Integer>("в т.ч. Село");
		OUT_Selo.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("OUT_Selo"));
		TableColumn<Components.tOsnova, Integer> IN_Perevod = new TableColumn<Components.tOsnova, Integer>("из др.");
		IN_Perevod.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("IN_Perevod"));
		TableColumn<Components.tOsnova, Integer> OUT_Perevod = new TableColumn<Components.tOsnova, Integer>("в др.");
		OUT_Perevod.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("OUT_Perevod"));
		TableColumn<Components.tOsnova, Integer> IN_Deti = new TableColumn<Components.tOsnova, Integer>("Поступило");
		IN_Deti.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("IN_Deti"));
		TableColumn<Components.tOsnova, Integer> OUT_Deti = new TableColumn<Components.tOsnova, Integer>("Выбыло");
		OUT_Deti.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("OUT_Deti"));
		TableColumn<Components.tOsnova, Integer> IN_Perevod_Deti = new TableColumn<Components.tOsnova, Integer>("из др.");
		IN_Perevod_Deti.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("IN_Perevod_Deti"));
		TableColumn<Components.tOsnova, Integer> OUT_Perevod_Deti = new TableColumn<Components.tOsnova, Integer>("в др.");
		OUT_Perevod_Deti.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("OUT_Perevod_Deti"));
		TableColumn<Components.tOsnova, Integer> IN_Mother = new TableColumn<Components.tOsnova, Integer>("Поступило");
		IN_Mother.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("IN_Mother"));
		TableColumn<Components.tOsnova, Integer> OUT_Mother = new TableColumn<Components.tOsnova, Integer>("Выбыло");
		OUT_Mother.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("OUT_Mother"));
		TableColumn<Components.tOsnova, Integer> IN_Perevod_Mother = new TableColumn<Components.tOsnova, Integer>("из др.");
		IN_Perevod_Mother.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("IN_Perevod_Mother"));
		TableColumn<Components.tOsnova, Integer> OUT_Perevod_Mother = new TableColumn<Components.tOsnova, Integer>("в др.");
		OUT_Perevod_Mother.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("OUT_Perevod_Mother"));
		TableColumn<Components.tOsnova, Integer> DIE = new TableColumn<Components.tOsnova, Integer>("Умерло");
		DIE.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("DIE"));
		TableColumn<Components.tOsnova, Integer> DIE_Deti = new TableColumn<Components.tOsnova, Integer>("умер");
		DIE_Deti.setCellValueFactory(new PropertyValueFactory<Components.tOsnova, Integer>("DIE_Deti"));
		Postupilo.getColumns().addAll(IN_All, IN_Selo);
		Vibilo.getColumns().addAll(OUT_All, OUT_Selo);
		Perevod.getColumns().addAll(IN_Perevod, OUT_Perevod);
		Deti.getColumns().addAll(IN_Deti, IN_Perevod_Deti, OUT_Deti, OUT_Perevod_Deti, DIE_Deti);
		Mother.getColumns().addAll(IN_Mother, IN_Perevod_Mother, OUT_Mother, OUT_Perevod_Mother);
		tab2.getColumns().setAll(Postupilo, Vibilo, Perevod, OUT_DrSt, Deti, Mother, DIE);
		for(int index=0;index<tab2.getColumns().size();index++){
			for(int j=0;j<tab2.getColumns().get(index).getColumns().size();j++){
				tab2.getColumns().get(index).getColumns().get(j).setSortable(false);
			}
			tab2.getColumns().get(index).setSortable(false);
		}	
	}
	@SuppressWarnings("unchecked")
	private void CreateTab_nu_ochen_nujna9() {
		tab_nu_ochen_nujna9.getItems().clear();
		masterDataTab_nu_ochen_nujna9.clear();
		TableColumn<Components.tnu_ochen_nujna9, String> Postupilo = new TableColumn<Components.tnu_ochen_nujna9, String>("Поступило");
		TableColumn<Components.tnu_ochen_nujna9, String> Vibilo = new TableColumn<Components.tnu_ochen_nujna9, String>("Выбыло");
		TableColumn<Components.tnu_ochen_nujna9, String> Perevod = new TableColumn<Components.tnu_ochen_nujna9, String>("Переводы");
		TableColumn<Components.tnu_ochen_nujna9, Integer> OUT_DrSt = new TableColumn<Components.tnu_ochen_nujna9, Integer>("др. Стационар");
		OUT_DrSt.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("OUT_DrSt"));
		TableColumn<Components.tnu_ochen_nujna9, String> Deti = new TableColumn<Components.tnu_ochen_nujna9, String>("Дети до года");
		TableColumn<Components.tnu_ochen_nujna9, String> Mother = new TableColumn<Components.tnu_ochen_nujna9, String>("Матери");

		TableColumn<Components.tnu_ochen_nujna9, String> Koika = new TableColumn<Components.tnu_ochen_nujna9, String>("Койка");
		Koika.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, String>("Name"));
		TableColumn<Components.tnu_ochen_nujna9, Integer> IN_All = new TableColumn<Components.tnu_ochen_nujna9, Integer>("Всего");
		IN_All.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("IN_All"));
		IN_All.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> IN_Selo = new TableColumn<Components.tnu_ochen_nujna9, Integer>("в т.ч. Село");
		IN_Selo.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("IN_Selo"));
		IN_Selo.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> OUT_All = new TableColumn<Components.tnu_ochen_nujna9, Integer>("Всего");
		OUT_All.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("OUT_All"));
		OUT_All.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> OUT_Selo = new TableColumn<Components.tnu_ochen_nujna9, Integer>("в т.ч. Село");
		OUT_Selo.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("OUT_Selo"));
		OUT_Selo.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> IN_Perevod = new TableColumn<Components.tnu_ochen_nujna9, Integer>("из др.");
		IN_Perevod.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("IN_Perevod"));
		IN_Perevod.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> OUT_Perevod = new TableColumn<Components.tnu_ochen_nujna9, Integer>("в др.");
		OUT_Perevod.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("OUT_Perevod"));
		OUT_Perevod.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> IN_Deti = new TableColumn<Components.tnu_ochen_nujna9, Integer>("Поступило");
		IN_Deti.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("IN_Deti"));
		IN_Deti.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> OUT_Deti = new TableColumn<Components.tnu_ochen_nujna9, Integer>("Выбыло");
		OUT_Deti.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("OUT_Deti"));
		OUT_Deti.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> IN_Perevod_Deti = new TableColumn<Components.tnu_ochen_nujna9, Integer>("из др.");
		IN_Perevod_Deti.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("IN_Perevod_Deti"));
		IN_Perevod_Deti.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> OUT_Perevod_Deti = new TableColumn<Components.tnu_ochen_nujna9, Integer>("в др.");
		OUT_Perevod_Deti.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("OUT_Perevod_Deti"));
		OUT_Perevod_Deti.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> IN_Mother = new TableColumn<Components.tnu_ochen_nujna9, Integer>("Поступило");
		IN_Mother.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("IN_Mother"));
		IN_Mother.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> OUT_Mother = new TableColumn<Components.tnu_ochen_nujna9, Integer>("Выбыло");
		OUT_Mother.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("OUT_Mother"));
		OUT_Mother.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> IN_Perevod_Mother = new TableColumn<Components.tnu_ochen_nujna9, Integer>("из др.");
		IN_Perevod_Mother.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("IN_Perevod_Mother"));
		IN_Perevod_Mother.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> OUT_Perevod_Mother = new TableColumn<Components.tnu_ochen_nujna9, Integer>("в др.");
		OUT_Perevod_Mother.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("OUT_Perevod_Mother"));
		OUT_Perevod_Mother.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> DIE = new TableColumn<Components.tnu_ochen_nujna9, Integer>("Умерло");
		DIE.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("DIE"));
		DIE.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		TableColumn<Components.tnu_ochen_nujna9, Integer> DIE_Deti = new TableColumn<Components.tnu_ochen_nujna9, Integer>("умер");
		DIE_Deti.setCellValueFactory(new PropertyValueFactory<Components.tnu_ochen_nujna9, Integer>("DIE_Deti"));
		DIE_Deti.setCellFactory(new Callback<TableColumn<Components.tnu_ochen_nujna9, Integer>, TableCell<Components.tnu_ochen_nujna9, Integer>>() {
			@Override
			public TableCell<tnu_ochen_nujna9, Integer> call(TableColumn<tnu_ochen_nujna9, Integer> param) {
				return new StyleValueCellTab_nu_ochen_nujna9();
			}});
		Postupilo.getColumns().addAll(IN_All, IN_Selo);
		Vibilo.getColumns().addAll(OUT_All, OUT_Selo);
		Perevod.getColumns().addAll(IN_Perevod, OUT_Perevod);
		Deti.getColumns().addAll(IN_Deti, IN_Perevod_Deti, OUT_Deti, OUT_Perevod_Deti, DIE_Deti);
		Mother.getColumns().addAll(IN_Mother, IN_Perevod_Mother, OUT_Mother, OUT_Perevod_Mother);
		tab_nu_ochen_nujna9.getColumns().setAll(Koika,Postupilo, Vibilo, Perevod, OUT_DrSt, Deti, Mother, DIE);
		for(int index=0;index<tab_nu_ochen_nujna9.getColumns().size();index++){
			for(int j=0;j<tab_nu_ochen_nujna9.getColumns().get(index).getColumns().size();j++){
				tab_nu_ochen_nujna9.getColumns().get(index).getColumns().get(j).setSortable(false);
			}
			tab_nu_ochen_nujna9.getColumns().get(index).setSortable(false);
		}		
	}
	
	private void ViewTable(int otdelenie, LocalDate date) {
		try {
			table.getItems().clear();
			table.refresh();
			masterData.clear();
			dataidosnova.clear();

			String sql = "";
			if (date.isBefore(LocalDate.now())) {
				// изменили дату на меньшую
				sql = "SELECT Name,Vsego,Vsego_Deti,Vsego_Selo,Vsego_Mother,Flag,vs.id_OK AS id_OK,Chislo_Koek, vs.id_Osnova AS id_Osnova,Vsego-Nachalo AS Nachalo,Vsego_Deti-Nachalo_Deti AS Nachalo_Deti,Vsego_Mother-Nachalo_Mother AS Nachalo_Mother "
						+ "FROM (SELECT Name,OK.id_OK AS id_OK,Chislo_Koek FROM OK INNER JOIN Koiki ON OK.id_Koiki = Koiki.id_Koiki "
						+ "WHERE id_Otdelenie = " + otdelenie + " AND '"+calendar.getValue()+"' BETWEEN OK.Date_Open AND OK.Date_Close" 
						+ ") k LEFT JOIN ("
						+ "SELECT Vsego,Vsego_Deti,Vsego_Selo,Vsego_Mother,Flag,v.id_OK AS id_OK,d.id_Osnova AS id_Osnova,Nachalo,Nachalo_Deti,Nachalo_Mother FROM (SELECT "
						+ "Vsego-ifnull(IN_ALL,0)+ifnull(OUT_ALL,0)-ifnull(IN_Perevod,0)+ifnull(OUT_Perevod,0)+ifnull(DIE,0) AS Vsego,"
						+ "Vsego_Deti-ifnull(IN_Deti,0)+ifnull(OUT_Deti,0)-ifnull(IN_Perevod_Deti,0)+ifnull(OUT_Perevod_Deti,0)+ifnull(DIE_Deti,0) AS Vsego_Deti,"
						+ "Vsego_Selo-ifnull(IN_Selo,0)+ifnull(OUT_Selo,0) AS Vsego_Selo,"
						+ "Vsego_Mother-ifnull(IN_Mother,0)+ifnull(OUT_Mother,0)-ifnull(IN_Perevod_Mother,0)+ifnull(OUT_Perevod_Mother,0) AS Vsego_Mother,"
						+ "reg.id_OK AS id_OK FROM (SELECT Vsego,Vsego_Deti,Vsego_Selo,Vsego_Mother,id_OK FROM (SELECT id_OK,id_Registr "
						+ "FROM OK WHERE id_Otdelenie = " + otdelenie +" AND '"+calendar.getValue()+"' BETWEEN Date_Open AND Date_Close"
						+ ") ok INNER JOIN("
						+ "SELECT Vsego,Vsego_Deti,Vsego_Selo,Vsego_Mother,id_Registr FROM  Registr ) r ON r.id_Registr = ok.id_Registr) reg "
						+ "LEFT JOIN (SELECT id_OK," + "SUM(IN_ALL) AS IN_ALL," + "SUM(IN_Deti) AS IN_Deti,"
						+ "SUM(IN_Selo) AS IN_Selo," + "SUM(IN_Mother) AS IN_Mother," + "SUM(IN_Perevod) AS IN_Perevod,"
						+ "SUM(IN_Perevod_Deti) AS IN_Perevod_Deti," + "SUM(IN_Perevod_Mother) AS IN_Perevod_Mother,"
						+ "SUM(OUT_ALL) AS OUT_ALL," + "SUM(OUT_Deti) AS OUT_Deti," + "SUM(OUT_Selo) AS OUT_Selo,"
						+ "SUM(OUT_Mother) AS OUT_Mother," + "SUM(OUT_Perevod) AS OUT_Perevod,"
						+ "SUM(OUT_Perevod_Deti) AS OUT_Perevod_Deti,"
						+ "SUM(OUT_Perevod_Mother) AS OUT_Perevod_Mother," + "SUM(DIE) AS DIE, "
						+ "SUM(DIE_Deti) AS DIE_Deti FROM Osnova "
						+ "WHERE Date > '" + calendar.getValue() + "' GROUP BY id_OK) osn ON reg.id_OK = osn.id_OK) v "
						+ "LEFT JOIN (SELECT id_OK,CASE WHEN Date = '" + calendar.getValue()
						+ "' THEN '1' ELSE '0' END AS Flag,id_Osnova," + "CASE WHEN Date = '" + calendar.getValue()
						+ "' THEN ifnull(IN_ALL,0)-ifnull(OUT_ALL,0)+ifnull(IN_Perevod,0)-ifnull(OUT_Perevod,0)-ifnull(DIE,0) ELSE 0 END AS Nachalo,"
						+ "CASE WHEN Date = '" + calendar.getValue()
						+ "' THEN ifnull(IN_Deti,0)-ifnull(OUT_Deti,0)+ifnull(IN_Perevod_Deti,0)-ifnull(OUT_Perevod_Deti,0)-ifnull(DIE_Deti,0) ELSE 0 END AS Nachalo_Deti,"
						+ "CASE WHEN Date = '" + calendar.getValue()
						+ "' THEN ifnull(IN_Mother,0)-ifnull(OUT_Mother,0)+ifnull(IN_Perevod_Mother,0)-ifnull(OUT_Perevod_Mother,0) ELSE 0 END AS Nachalo_Mother "
						+ "FROM Osnova " + "WHERE Date <= '" + calendar.getValue()
						+ "' GROUP BY id_OK,Date) d ON d.id_OK=v.id_OK) vs ON vs.id_OK=k.id_OK GROUP BY Name";

			} else {

				sql = "SELECT Name," + "Vsego," + "Vsego_Deti," + "Vsego_Selo," + "Vsego_Mother," + "k.id_OK AS id_OK,"
						+ "Chislo_Koek," + "osn.id_Osnova AS id_Osnova," + "CASE WHEN Date = '" + calendar.getValue()
						+ "' THEN '1' ELSE '0' END AS Flag," + "CASE WHEN Date = '" + calendar.getValue()
						+ "' THEN Vsego - "
						+ "(SELECT IN_ALL-OUT_ALL+IN_Perevod-OUT_Perevod-DIE AS Nachalo FROM Osnova " + "WHERE Date = '"
						+ calendar.getValue() + "' AND  id_Osnova = osn.id_Osnova) ELSE Vsego END AS Nachalo,"
						+ "CASE WHEN Date = '" + calendar.getValue() + "' THEN Vsego_Deti - "
						+ "(SELECT IN_Deti-OUT_Deti+IN_Perevod_Deti-OUT_Perevod_Deti-DIE_Deti AS Nachalo_Deti FROM Osnova "
						+ "WHERE Date = '" + calendar.getValue()
						+ "' AND  id_Osnova = osn.id_Osnova) ELSE Vsego_Deti END AS Nachalo_Deti,"
						+ "CASE WHEN Date = '" + calendar.getValue() + "' THEN Vsego_Mother - "
						+ "(SELECT IN_Mother-OUT_Mother+IN_Perevod_Mother-OUT_Perevod_Mother AS Nachalo_Mother FROM Osnova "
						+ "WHERE Date = '" + calendar.getValue()
						+ "' AND  id_Osnova = osn.id_Osnova) ELSE Vsego_Mother END AS Nachalo_Mother "
						+ "FROM (SELECT Name,Koiki.id_Koiki AS id_Koiki, OK.id_OK AS id_OK "
						+ "FROM OK INNER JOIN Koiki ON OK.id_Koiki = Koiki.id_Koiki  " + "WHERE id_Otdelenie = "
						+ otdelenie + " AND '"+calendar.getValue()+"' BETWEEN OK.Date_Open AND OK.Date_Close) k "
						+ "LEFT JOIN (SELECT Vsego,Vsego_Deti,Vsego_Selo,Vsego_Mother,o.id_OK AS id_OK,Date,Chislo_Koek,o.id_Osnova AS id_Osnova "
						+ "FROM (SELECT Vsego,Vsego_Deti,Vsego_Selo,Vsego_Mother,Chislo_Koek,id_OK FROM (SELECT id_OK,Chislo_Koek,id_Registr "
						+ "FROM OK WHERE id_Otdelenie = " + otdelenie +" AND '"+calendar.getValue()+"' BETWEEN Date_Open AND Date_Close"
						+ ") ok INNER JOIN(SELECT Vsego,Vsego_Deti,Vsego_Selo,Vsego_Mother,"
						+ "id_Registr FROM  Registr ) r "
						+ "ON r.id_Registr = ok.id_Registr) reg INNER JOIN (SELECT MAX(Date) AS Date,id_OK,id_Osnova FROM Osnova  "
						+ "WHERE Date <=  '" + calendar.getValue() + "' GROUP BY id_OK) o "
						+ "ON reg.id_OK = o.id_OK) osn ON osn.id_OK=k.id_OK GROUP BY Name";
			}

			log.debug(sql);

			ResultSet rs = conn.createStatement().executeQuery(sql);
			int I_Nachalo = 0;
			int I_Nachalo_Deti = 0;
			int I_Nachalo_Mother = 0;
			int I_Chislo_Koek = 0;
			int I_Vsego = 0;
			int I_Vsego_Deti = 0;
			int I_Vsego_Selo = 0;
			int I_Vsego_Mother = 0;
			while (rs.next()) {
				Components.tRegistr registr = new Components.tRegistr();
				registr.setName(rs.getString("Name"));
				registr.setNachalo(rs.getInt("Nachalo"));
				registr.setNachalo_Deti(rs.getInt("Nachalo_Deti"));
				registr.setNachalo_Mother(rs.getInt("Nachalo_Mother"));
				registr.setChislo_Koek(rs.getInt("Chislo_Koek"));
				registr.setVsego(rs.getInt("Vsego"));
				registr.setVsego_Deti(rs.getInt("Vsego_Deti"));
				registr.setVsego_Selo(rs.getInt("Vsego_Selo"));
				registr.setVsego_Mother(rs.getInt("Vsego_Mother"));
				registr.setFlag(rs.getBoolean("Flag"));
				registr.setid_OK(rs.getInt("id_OK"));
				I_Nachalo = I_Nachalo + rs.getInt("Nachalo");
				I_Nachalo_Deti = I_Nachalo_Deti + rs.getInt("Nachalo_Deti");
				I_Nachalo_Mother = I_Nachalo_Mother + rs.getInt("Nachalo_Mother");
				I_Chislo_Koek = I_Chislo_Koek + rs.getInt("Chislo_Koek");
				I_Vsego = I_Vsego + rs.getInt("Vsego");
				I_Vsego_Deti = I_Vsego_Deti + rs.getInt("Vsego_Deti");
				I_Vsego_Selo = I_Vsego_Selo + rs.getInt("Vsego_Selo");
				I_Vsego_Mother = I_Vsego_Mother + rs.getInt("Vsego_Mother");
				masterData.add(registr);
				dataidosnova.add(rs.getInt("id_Osnova"));
			}
			Components.tRegistr registr = new Components.tRegistr();
			registr.setName("ИТОГО");
			registr.setNachalo(I_Nachalo);
			registr.setNachalo_Deti(I_Nachalo_Deti);
			registr.setNachalo_Mother(I_Nachalo_Mother);
			registr.setChislo_Koek(I_Chislo_Koek);
			registr.setVsego(I_Vsego);
			registr.setVsego_Deti(I_Vsego_Deti);
			registr.setVsego_Selo(I_Vsego_Selo);
			registr.setVsego_Mother(I_Vsego_Mother);
			rs.close();
			masterData.add(registr);
			table.setItems(masterData);
			table.setRowFactory(new StyleRowFactory());
			table.getColumns().get(0).setMinWidth(200.0);
			ViewTab_nu_ochen_nujna9(otdelenie);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	private void ViewTab2(int id_Osnova) throws SQLException {
		tab2.getItems().clear();
		masterDataTab2.clear();
		String sql = "SELECT * FROM Osnova " + "WHERE id_Osnova=" + id_Osnova;
		log.debug(sql);
		ResultSet rs = conn.createStatement().executeQuery(sql);
		while (rs.next()) {
			Components.tOsnova osnova = new Components.tOsnova();
			osnova.setIN_All(rs.getInt("IN_All"));
			osnova.setIN_Selo(rs.getInt("IN_Selo"));
			osnova.setOUT_All(rs.getInt("OUT_All"));
			osnova.setOUT_Selo(rs.getInt("OUT_Selo"));
			osnova.setIN_Perevod(rs.getInt("IN_Perevod"));
			osnova.setIN_Perevod_Deti(rs.getInt("IN_Perevod_Deti"));
			osnova.setIN_Perevod_Mother(rs.getInt("IN_Perevod_Mother"));
			osnova.setOUT_Perevod(rs.getInt("OUT_Perevod"));
			osnova.setOUT_Perevod_Deti(rs.getInt("OUT_Perevod_Deti"));
			osnova.setOUT_Perevod_Mother(rs.getInt("OUT_Perevod_Mother"));
			osnova.setOUT_DrSt(rs.getInt("OUT_DrSt"));
			osnova.setIN_Deti(rs.getInt("IN_Deti"));
			osnova.setOUT_Deti(rs.getInt("OUT_Deti"));
			osnova.setIN_Mother(rs.getInt("IN_Mother"));
			osnova.setOUT_Mother(rs.getInt("OUT_Mother"));
			osnova.setDIE(rs.getInt("DIE"));
			osnova.setDIE_Deti(rs.getInt("DIE_Deti"));
			// osnova.setIN_AllL(rs.getInt("IN_All"));
			masterDataTab2.add(osnova);
		}
		rs.close();
		tab2.setItems(masterDataTab2);
		tab2.setRowFactory(new StyleRowFactoryTab2());
	}
	private void ViewTab_nu_ochen_nujna9(int otdelenie) throws SQLException {
		tab_nu_ochen_nujna9.getItems().clear();
		masterDataTab_nu_ochen_nujna9.clear();
//		String sql = "SELECT * FROM (SELECT * FROM Osnova WHERE id_Osnova IN ("+dataidosnova.get(0);
//		for(int index =1;index<dataidosnova.size();index ++){
//			sql=sql+","+dataidosnova.get(index);
//		}
//		sql=sql+")) a INNER  JOIN (SELECT Name,OK.id_OK AS id_OK,Chislo_Koek FROM OK INNER JOIN Koiki ON OK.id_Koiki = Koiki.id_Koiki "
//				+ "WHERE id_Otdelenie = "+otdelenie+") k ON a.id_OK=k.id_OK GROUP BY Name";
		String sql = "SELECT * FROM (SELECT Name,OK.id_OK AS id_OK FROM OK INNER JOIN Koiki ON OK.id_Koiki = Koiki.id_Koiki "
				+ "WHERE id_Otdelenie = "+otdelenie+ " AND '"+calendar.getValue()+"' BETWEEN OK.Date_Open AND OK.Date_Close) k LEFT JOIN(SELECT * FROM Osnova "
						+ "WHERE Date = '"+ calendar.getValue()+"') a ON a.id_OK=k.id_OK GROUP BY Name";
		ResultSet rs = conn.createStatement().executeQuery(sql);
		while (rs.next()) {
			Components.tnu_ochen_nujna9 nu_ochen_nujna9 = new Components.tnu_ochen_nujna9();
			nu_ochen_nujna9.setName(rs.getString("Name"));
			nu_ochen_nujna9.setIN_All(rs.getInt("IN_All"));
			nu_ochen_nujna9.setIN_Selo(rs.getInt("IN_Selo"));
			nu_ochen_nujna9.setOUT_All(rs.getInt("OUT_All"));
			nu_ochen_nujna9.setOUT_Selo(rs.getInt("OUT_Selo"));
			nu_ochen_nujna9.setIN_Perevod(rs.getInt("IN_Perevod"));
			nu_ochen_nujna9.setIN_Perevod_Deti(rs.getInt("IN_Perevod_Deti"));
			nu_ochen_nujna9.setIN_Perevod_Mother(rs.getInt("IN_Perevod_Mother"));
			nu_ochen_nujna9.setOUT_Perevod(rs.getInt("OUT_Perevod"));
			nu_ochen_nujna9.setOUT_Perevod_Deti(rs.getInt("OUT_Perevod_Deti"));
			nu_ochen_nujna9.setOUT_Perevod_Mother(rs.getInt("OUT_Perevod_Mother"));
			nu_ochen_nujna9.setOUT_DrSt(rs.getInt("OUT_DrSt"));
			nu_ochen_nujna9.setIN_Deti(rs.getInt("IN_Deti"));
			nu_ochen_nujna9.setOUT_Deti(rs.getInt("OUT_Deti"));
			nu_ochen_nujna9.setIN_Mother(rs.getInt("IN_Mother"));
			nu_ochen_nujna9.setOUT_Mother(rs.getInt("OUT_Mother"));
			nu_ochen_nujna9.setDIE(rs.getInt("DIE"));
			nu_ochen_nujna9.setDIE_Deti(rs.getInt("DIE_Deti"));
			// osnova.setIN_AllL(rs.getInt("IN_All"));
			masterDataTab_nu_ochen_nujna9.add(nu_ochen_nujna9);
		}
		rs.close();
		tab_nu_ochen_nujna9.setItems(masterDataTab_nu_ochen_nujna9);
		tab_nu_ochen_nujna9.setRowFactory(new StyleRowFactoryTab_nu_ochen_nujna9());
//		tab_nu_ochen_nujna9.getColumns().get(2).get
//		tab_nu_ochen_nujna9.getItems().get(0)
	}
	
	@FXML
	public void ComboAction() throws SQLException {
		ViewTable(dataidotdelenia.get(ViborOtdeleni9.getSelectionModel().getSelectedIndex()), calendar.getValue());
		tab2.getItems().clear();
	}
	@FXML
	public void DatePickerAction() throws SQLException {
		tab2.getItems().clear();
		if (!ViborOtdeleni9.getSelectionModel().isEmpty()) {
			ViewTable(dataidotdelenia.get(ViborOtdeleni9.getSelectionModel().getSelectedIndex()), calendar.getValue());
		}
	}

	private void print() throws SQLException {
		PrinterJob printerJob = PrinterJob.createPrinterJob();
		if (printerJob.showPageSetupDialog(null) && printerJob.printPage(table)) {
			printerJob.endJob();
		}
		if (!ViborOtdeleni9.getSelectionModel().isEmpty() & !table.getSelectionModel().isEmpty()) {
			ViewTable(dataidotdelenia.get(ViborOtdeleni9.getSelectionModel().getSelectedIndex()), calendar.getValue());
		}
	}

	private void InitializeMenuItemToButton() {
		final ContextMenu contextMenu = new ContextMenu();
		MenuItem item1 = new MenuItem("Печать");
		item1.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				try {
					print();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		MenuItem item2 = new MenuItem("Обновить");
		item2.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				ViborOtdeleni9.setItems(FXCollections.observableArrayList(Get_List()));
				calendar.setValue(LocalDate.now());
				table.getItems().clear();
				tab2.getItems().clear();
			}
		});
		contextMenu.getItems().addAll(item1, item2);
		table.setContextMenu(contextMenu);
	}

	private ObservableList<String> Get_List() {
		try {
			dataidotdelenia.clear();
			String Queary = "SELECT Name,id_Otdelenie FROM Otdelenie WHERE date('now') BETWEEN Date_Open AND Date_Close GROUP BY Name";
			ResultSet rs = conn.createStatement().executeQuery(Queary);
			ObservableList<String> row = FXCollections.observableArrayList();
			while (rs.next()) {
				row.add(rs.getString(1));
				dataidotdelenia.add(Integer.valueOf(rs.getInt(2)));
			}
			return row;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	class StyleRowFactory implements Callback<TableView<Components.tRegistr>, TableRow<Components.tRegistr>> {
		@Override
		public TableRow<Components.tRegistr> call(TableView<Components.tRegistr> tableView) {
			return new TableRow<Components.tRegistr>() {
				@Override
				protected void updateItem(Components.tRegistr registr, boolean b) {
					super.updateItem(registr, b);
					if (registr == null) {
						setStyle(null);
						return;
					};
					if (registr.getFlag()) {
						setStyle("-fx-background-color: Orchid;");
					} else {
						if (registr.getName() == "ИТОГО") {
							setStyle("-fx-background-color: DarkKhaki; -fx-font-weight: bold;");
						} else {
							setStyle(null);
						}
					}
				}
			};
		}
	}
	class StyleRowFactoryTab2 implements Callback<TableView<Components.tOsnova>, TableRow<Components.tOsnova>> {
		@Override
		public TableRow<Components.tOsnova> call(TableView<Components.tOsnova> tableView) {
			return new TableRow<Components.tOsnova>() {
				@Override
				protected void updateItem(Components.tOsnova osnova, boolean b) {
					super.updateItem(osnova, b);
					if (osnova == null) {
						setStyle(null);
						return;
					};
//					setStyle("-fx-font-weight: bold;");
					setStyle(null);
				}
			};
		}
	}
	class StyleRowFactoryTab_nu_ochen_nujna9 implements Callback<TableView<Components.tnu_ochen_nujna9>, TableRow<Components.tnu_ochen_nujna9>> {
		@Override
		public TableRow<Components.tnu_ochen_nujna9> call(TableView<Components.tnu_ochen_nujna9> tableView) {
			return new TableRow<Components.tnu_ochen_nujna9>() {
				@Override
				protected void updateItem(Components.tnu_ochen_nujna9 nu_ochen_nujna9, boolean b) {
					super.updateItem(nu_ochen_nujna9, b);
					if (nu_ochen_nujna9 == null) {
						setStyle(null);
						return;
					};
					if (table.getItems().get(super.getIndex()).getFlag()) {
						setStyle("-fx-background-color: Orchid;");
					} else {
						setStyle(null);
					}
				}
			};
		}
	}
	//@Singleton
	class StyleValueCell extends TableCell<Components.tRegistr, Integer>{
		protected void updateItem(Integer item, boolean empty) {
			super.updateItem(item, empty);
			if (item == null) {
				setStyle(null);
				return;
			};
			if (item instanceof Integer) {
				if (item!=0){
					setStyle("-fx-border-color: black;-fx-border-width: 0 1 0 1; -fx-font-weight: bold;-fx-alignment: center;");
				} else {
					//setStyle(null);
					setStyle("-fx-border-color: black;-fx-border-width: 0 1 0 1; -fx-font-weight: bold;-fx-alignment: center;");
				}
			}
			super.setText(item.toString());
		}
	}
	class StyleValueCellTab_nu_ochen_nujna9 extends TableCell<tnu_ochen_nujna9, Integer>{
		protected void updateItem(Integer item, boolean empty) {
			super.updateItem(item, empty);
			if (item == null) {
				setStyle(null);
				return;
			};
			if (item instanceof Integer) {
				if (item!=0){
					setStyle("-fx-font-weight: bold;");
				} else {
					setStyle(null);
				}
			}
	super.setText(item.toString());
		}
	}
	
	interface Printable {
		void print(String s);
	}

	private void Open_Osnova_Insert() {
		try {
			if (!ViborOtdeleni9.getSelectionModel().isEmpty()) {
				if (table.getSelectionModel().getSelectedItem().getFlag()) {
					// Update
					FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/Osnova_Insert.fxml"));
					Parent root = (Parent) loader.load();
					Stage stage = new Stage();
					stage.initModality(Modality.APPLICATION_MODAL);
					// stage.initStyle(StageStyle.UNDECORATED);
					stage.setTitle(
							"Движение по " + table.getSelectionModel().getSelectedItem().getName().toString() + " на "
									+ calendar.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")).toString());
					stage.setScene(new Scene(root));
					Osnova_Insert controller = (Osnova_Insert) loader.getController();
					controller.setParent(this);
					controller.setTypeFormi(false);
					controller.setId_Otdelenie(
							dataidotdelenia.get(ViborOtdeleni9.getSelectionModel().getSelectedIndex()));
					controller.InsertTextInLabel(table.getSelectionModel().getSelectedItem().getName().toString());
					controller.setId_OK(table.getSelectionModel().getSelectedItem().getid_OK());
					controller.setId_Osnova(dataidosnova.get(table.getSelectionModel().getSelectedIndex()));
					controller.setDate(calendar.getValue().toString());
					controller.setUpdate_IN_ALL(tab2.getItems().get(0).getIN_All());
					controller.setUpdate_OUT_All(tab2.getItems().get(0).getOUT_All());
					controller.setUpdate_IN_Selo(tab2.getItems().get(0).getIN_Selo());
					controller.setUpdate_OUT_Selo(tab2.getItems().get(0).getOUT_Selo());
					controller.setUpdate_OUT_DrSt(tab2.getItems().get(0).getOUT_DrSt());
					controller.setUpdate_IN_Deti(tab2.getItems().get(0).getIN_Deti());
					controller.setUpdate_OUT_Deti(tab2.getItems().get(0).getOUT_Deti());

					controller.setUpdate_IN_Perevod(tab2.getItems().get(0).getIN_Perevod());
					controller.setUpdate_OUT_Perevod(tab2.getItems().get(0).getOUT_Perevod());
					controller.setUpdate_IN_Perevod_Deti(tab2.getItems().get(0).getIN_Perevod_Deti());
					controller.setUpdate_OUT_Perevod_Deti(tab2.getItems().get(0).getOUT_Perevod_Deti());
					controller.setUpdate_IN_Mother(tab2.getItems().get(0).getIN_Mother());
					controller.setUpdate_OUT_Mother(tab2.getItems().get(0).getOUT_Mother());
					controller.setUpdate_IN_Perevod_Mother(tab2.getItems().get(0).getIN_Perevod_Mother());
					controller.setUpdate_OUT_Perevod_Mother(tab2.getItems().get(0).getOUT_Perevod_Mother());
					controller.setUpdate_DIE(tab2.getItems().get(0).getDIE());
					controller.setUpdate_DIE_Deti(tab2.getItems().get(0).getDIE_Deti());

					controller.setDialogstage(stage);
					stage.showAndWait();
				} else {
					// Insert
					FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI_FX_Forms/Osnova_Insert.fxml"));
					Parent root = (Parent) loader.load();
					Stage stage = new Stage();
					stage.initModality(Modality.APPLICATION_MODAL);
					// stage.initStyle(StageStyle.UNDECORATED);
					stage.setTitle(
							"Движение по " + table.getSelectionModel().getSelectedItem().getName().toString() + " на "
									+ calendar.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")).toString());
					stage.setScene(new Scene(root));
					Osnova_Insert controller = (Osnova_Insert) loader.getController();
					controller.setParent(this);
					controller.setTypeFormi(true);
					controller.setId_Otdelenie(
							dataidotdelenia.get(ViborOtdeleni9.getSelectionModel().getSelectedIndex()));
					controller.InsertTextInLabel(table.getSelectionModel().getSelectedItem().getName().toString());
					controller.setId_OK(table.getSelectionModel().getSelectedItem().getid_OK());
					controller.setDate(calendar.getValue().toString());
					controller.setDialogstage(stage);
					stage.show();

				}
			}
		} catch (IOException | SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	public void mouseClicked(MouseEvent e) throws SQLException {
		if (!table.getSelectionModel().isEmpty()) {
			if (!table.getSelectionModel().getSelectedItem().getFlag()) {
				tab2.getItems().clear();
			} else {
				ViewTab2(dataidosnova.get(table.getSelectionModel().getSelectedIndex()));
			}
			if (e.getClickCount() == 1) {
				tab_nu_ochen_nujna9.getSelectionModel().select(table.getSelectionModel().getSelectedIndex());
			}
			if (e.getClickCount() == 2) {
				if (table.getSelectionModel().getSelectedItem().getName() != "ИТОГО") {
					Open_Osnova_Insert();
				}
			}
		}
	}

	public int getDataidok() {
		return dataidok;
	}

	public void setDataidok(int dataidok) {
		this.dataidok = dataidok;
	}

	@FXML
	public void onPrev(ActionEvent event) {
		calendar.setValue(calendar.getValue().minusDays(1));
		if (!ViborOtdeleni9.getSelectionModel().isEmpty()) {
			ViewTable(dataidotdelenia.get(ViborOtdeleni9.getSelectionModel().getSelectedIndex()),calendar.getValue());
		}
		tab2.getItems().clear();
	}

	@FXML
	public void onNext(ActionEvent event) {
		calendar.setValue(calendar.getValue().plusDays(1));
		if (!ViborOtdeleni9.getSelectionModel().isEmpty()) {
			ViewTable(dataidotdelenia.get(ViborOtdeleni9.getSelectionModel().getSelectedIndex()),calendar.getValue());
		}
		tab2.getItems().clear();
	}

	public void retry() throws SQLException {
		if (!ViborOtdeleni9.getSelectionModel().isEmpty()) {
			ViewTable(dataidotdelenia.get(ViborOtdeleni9.getSelectionModel().getSelectedIndex()), calendar.getValue());
		} else {
			table.getItems().clear();
		}
		tab2.getItems().clear();
		tab_nu_ochen_nujna9.getItems().clear();
	}

	public Stage getThisdialogstage() {
		return thisdialogstage;
	}

	public void setThisdialogstage(Stage thisdialogstage) {
		this.thisdialogstage = thisdialogstage;
	}

	@FXML
	public void KeyPress(KeyEvent key) {
		if (key.getCode() == KeyCode.ENTER) {
			if (table.getSelectionModel().getSelectedItem().getName() != "ИТОГО") {
				Open_Osnova_Insert();
			}
		}
		if (key.getCode() == KeyCode.LEFT) {
			onPrev(null);
		}
		if (key.getCode() == KeyCode.RIGHT) {
			onNext(null);
		}
	}
}
