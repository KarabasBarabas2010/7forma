package GUI_Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

import org.apache.logging.log4j.Logger;

import ConnectManager.LoggerAllAplication;
import ConnectManager.SQLiteJDBC;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class OK_Insert implements Initializable{
	private static Logger log = LoggerAllAplication.getlogger();
	private OK Parent;
	private Stage thisdialogstage;
	private Stage Parentdialogstage;
	private Integer id_Otdelenie;
	private ObservableList<Integer> dataidok = FXCollections.observableArrayList();
	
	Connection conn = SQLiteJDBC.Get_Conn();
	
	@FXML private ComboBox<String> Name;
	@FXML private TextField Chislo_Koek; 
	@FXML private TextField Plan_KD; 
	@FXML private DatePicker Date_Open;
	@FXML private DatePicker Date_Close;
	@FXML private Button btnInsert;
	
	@FXML public void KeyPress(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){
			getDialogstage().close();			
		}
	}

	@Override
 	public void initialize(URL location, ResourceBundle resources) {
		Name.setItems(FXCollections.observableArrayList(Get_List()));
	}
	
	public OK getParent() {
		return Parent;
	}

	public void setParent(OK parent) {
		this.Parent = parent;
	}

	public Stage getDialogstage() {
		return thisdialogstage;
	}
	public void setDialogstage(Stage dialogstage) {
		this.thisdialogstage = dialogstage;
	}
	public Stage getParentdialogstage() {
		return Parentdialogstage;
	}
	public void setParentdialogstage(Stage parentdialogstage) {
		this.Parentdialogstage = parentdialogstage;
	}
	public Integer getId_Otdelenie() {
		return id_Otdelenie;
	}
	public void setId_Otdelenie(Integer id_Otdelenie) {
		this.id_Otdelenie = id_Otdelenie;
	}
	private boolean isDataNull(LocalDate date){
		return date == null;
	}
	private boolean isNull(final String str){
		return str == null || str.isEmpty() || str.equals("null");
	}
	
	@FXML public void onInsert(ActionEvent event) throws SQLException{
			
		String query = "INSERT INTO Registr (Vsego,Vsego_Deti,Vsego_Selo,Vsego_Mother,Date) VALUES (0,0,0,0,'"
				+(isDataNull(Date_Close.getValue()) ? LocalDate.now() : Date_Close.getValue())+"')";
		conn.createStatement().executeUpdate(query);		
		
		String sql="INSERT INTO OK (id_Otdelenie,id_Koiki,Chislo_Koek,Plan_KD,Date_Open,Date_Close,id_Registr) VALUES (?,?,?,?,?,?,"
				+ "(SELECT seq FROM sqlite_sequence WHERE name='Registr'))";
		log.debug(sql);
		PreparedStatement pstmt = conn.prepareStatement(sql);
		
		pstmt.setInt(1,getId_Otdelenie());
		pstmt.setInt(2, (dataidok.get(Name.getSelectionModel().getSelectedIndex())));
		pstmt.setString(3,(isNull(Chislo_Koek.getText()) ? "0" : Chislo_Koek.getText()));
		pstmt.setString(4,(isNull(Plan_KD.getText()) ? "0" : Plan_KD.getText()));
		pstmt.setString(5,isDataNull(Date_Open.getValue()) ? "1900-01-01" : Date_Open.getValue().toString());
		pstmt.setString(6,isDataNull(Date_Close.getValue()) ? "9999-12-31" : Date_Close.getValue().toString());
		
		pstmt.executeUpdate();

		pstmt.close();
		conn.close();
		getDialogstage().close();	
		getParent().retry();
	}
	private ObservableList<String> Get_List() {
		try {
			dataidok.clear();
			String Queary ="SELECT Name,id_Koiki FROM Koiki GROUP BY Name";
			Connection conn = SQLiteJDBC.Get_Conn();
			ResultSet rs = conn.createStatement().executeQuery(Queary);
			ObservableList<String> row = FXCollections.observableArrayList();
			while (rs.next()) {
				row.add(rs.getString(1));
				dataidok.add(rs.getInt(2));
			}
			return row;			
		} catch (SQLException e) {
			log.error(e);
			return null;
		}
	}
	
}